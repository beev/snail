import React from 'react';
import ReactDOM from 'react-dom';
import './css/index.css';
import "leaflet/dist/leaflet.css";
import "typeface-roboto";


import Areas from './classes/Areas';
import LoginForm from './classes/LoginForm';
import RegisterForm from './classes/RegisterForm';
import HomePage from './classes/HomePage'
import UserProfile from './classes/UserProfile';
import UserSettings from './classes/UserSettings';
import Posts from './classes/Posts'
import Rides from './classes/Rides'
import Navbar from './classes/Navbar'
import * as serviceWorker from './serviceWorker';
import HowToUse from './classes/HowToUse';

/**home page */
if (window.location.pathname === '/' || window.location.pathname === '/logout')
    ReactDOM.render(
        <div>
            <Navbar auth={false} />
            <HomePage />
        </div>, document.getElementById('main'));

/**login */
else if (window.location.pathname === '/login')
    ReactDOM.render(
        <div>
            <Navbar back={true} />
            <LoginForm />
        </div>, document.getElementById('main'));

/**register */
else if (window.location.pathname === '/register')
    ReactDOM.render(
        <div>
            <Navbar back={true} />
            <RegisterForm />
        </div>, document.getElementById('main'));

/**how to use */
else if (window.location.pathname === '/howToUse')
    ReactDOM.render(
        <div>
            <Navbar back={true} />
            <HowToUse />
        </div>, document.getElementById('main'));

/**areas */
else if (window.location.pathname === '/areas')
    ReactDOM.render(
        <div>
            <Navbar auth={true} />
            <Areas />
        </div>, document.getElementById('main'));

/**user profile */
else if (window.location.pathname.match(/\/users\/(\d+)/))
    ReactDOM.render(
        <div>
            <Navbar auth={true} profile={true} />
            <UserProfile />
        </div>, document.getElementById('main'));

/**user settings */
else if (window.location.pathname === '/vehicle')
    ReactDOM.render(
        <div>
            <Navbar auth={true} profile={true}/>
            <UserSettings content='vehicle' />
        </div>, document.getElementById('main'));

else if (window.location.pathname === '/home')
    ReactDOM.render(
        <div>
            <Navbar auth={true} profile={true}/>
            <UserSettings content='home' />
        </div>, document.getElementById('main'));

else if (window.location.pathname === '/username')
    ReactDOM.render(
        <div>
            <Navbar auth={true} profile={true}/>
            <UserSettings content='username' />
        </div>, document.getElementById('main'));

else if (window.location.pathname === '/password')
    ReactDOM.render(
        <div>
            <Navbar auth={true} profile={true}/>
            <UserSettings content='password' />
        </div>, document.getElementById('main'));

else if (window.location.pathname === '/avatar')
    ReactDOM.render(
        <div>
            <Navbar auth={true} profile={true}/>
            <UserSettings content='avatar' />
        </div>, document.getElementById('main'));

/**posts */
else if (window.location.pathname === '/posts')
    ReactDOM.render(
        <div>
            <Navbar auth={true} />
            <Posts main='new' />
        </div>, document.getElementById('main'));

else if (window.location.pathname === '/postsShell')
    ReactDOM.render(
        <div>
            <Navbar auth={true} />
            <Posts main='shell' />
        </div>, document.getElementById('main'));

else if (window.location.pathname === '/postsSlug')
    ReactDOM.render(
        <div>
            <Navbar auth={true} />
            <Posts main='slug' />
        </div>, document.getElementById('main'));

/**ride */
else if (window.location.pathname.match(/\/rides\/(\d+)/)) {
    //get rid
    var str = window.location.pathname
    var numb = str.match(/\d/g);
    numb = numb.join("");
    
    ReactDOM.render(
        <div>
            <Navbar auth={true} />
            <Rides rid={numb} />
        </div>, document.getElementById('main'));
}


// If you want your app to work offline and load faster, you can change
// unregister() to register() below. Note this comes with some pitfalls.
// Learn more about service workers: https://bit.ly/CRA-PWA
serviceWorker.unregister();
