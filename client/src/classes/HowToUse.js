import React, { Component } from 'react';

class HowToUse extends Component {

    constructor(props) {
        super(props);
    }

    render() {
        return (
            <div className="row d-flex justify-content-center px-3 py-3 my-2" id="howto">
                <h2 className=" mb-5">Let's show you how to get this done!</h2>
                <p>People giving a ride are represented by the slug, and the ones who need a ride are the shells. That means that</p>
                <p className="d-flex flex-column">if you need a ride, you click here</p>
                <img src="../../icons/howToUse/see-shell.png" alt="view-shell" className="center w-75"></img>

                <p className="pt-5">and this is where you go if you wanna give one</p>
                <img src="../../icons/howToUse/see-slug.png" alt="view-slug" className="center w-75"></img>

                <p className="pt-5">if you want to make your own post, go here. This is also the home page</p>
                <img src="../../icons/howToUse/make-new.png" alt="new" className="center w-75"></img>

                <p className="pt-5">To join a post, simply click here</p>
                <img src="../../icons/howToUse/join-post.png" alt="join" className="center w-75"></img>

            </div>
        )
    }

}

export default HowToUse;
