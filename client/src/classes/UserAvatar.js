import React from 'react';
import Avatar from 'react-avatar';

class UserAvatar extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            avatar: null
        };
    }


    componentDidMount() {
        var urlCreator = window.URL || window.webkitURL;
        var blobURL;
        /** 
        fetch('/users/avatar/' + this.props.UID)
            .then(res => res.blob())
            .then((blob) => {
                    blobURL = urlCreator.createObjectURL(blob);
                    this.setState({ avatar: null });
                    console.log(blobURL)
            })*/
    };

    render() {
        return (
            this.state.avatar !== null ?
                <img src={this.state.avatar} id='avatar' />
                :
                <Avatar id='avatar' size={this.props.size} round name={this.props.name} />
        );
    }
}
export default UserAvatar;
