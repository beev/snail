import React, { Component } from 'react';

class ErrorPage extends Component {

  constructor(props) {
    super(props);
    this.state = { result: [] };
  }

  render() {
      return(
          <div className="row d-flex justify-content-center mt-0" id="error">
            <h2 className="px-2 mb-5">Not everything is alright right now.</h2>
                    <img src="../../icons/error.png" alt="error" className="center w-75"></img>
                </div>
      )
  }
  
}

export default ErrorPage;
