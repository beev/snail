import React from 'react'
import Cookies from 'js-cookie';
import axios from 'axios';

import UserAvatar from './UserAvatar'

import '../css/profile.css';
import '../css/index.css';
import ErrorPage from './ErrorPage';

class UserProfile extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            result: [],
            selectedFile: null,
            loaded: 0,
            avatar: '',
            userID: 0
        };
    }

    componentDidMount() {
        var userID = JSON.parse(Cookies.get('loggedInUser'));
        this.setState({ userID });
        let url = "/users/" + userID;
        fetch(url)
            .then(res => res.json())
            .then(data => this.setState({ result: data[0] }))
    }

    submitAvatar = (event) => {
        event.preventDefault();

        const data = new FormData()

        let url = "/users";
        this.setState({
            selectedFile: event.target.files[0],
            loaded: 0,
        }, function () { data.append('file', this.state.selectedFile) });

        url = url + "/avatar/" + this.state.userID;
        axios.put(url, data, {
            onUploadProgress: ProgressEvent => {
                this.setState({ loaded: (ProgressEvent.loaded / ProgressEvent.total * 100) })
            },
        })
            .then(res => {
                if (this.state.loaded === 100)
                    window.location.href = '/users/' + this.state.userID;
            })
    }

    deleteAvatar = (avatar, event) => {
        event.preventDefault();

        let url = "/users" + "/avatar/" + this.state.userID + "?avatarName=" + avatar;
        fetch(url, { method: 'DELETE' })
            .then(window.location.href = '/users/' + this.state.userID)
    }

    render() {
        var i;
        if (JSON.stringify(Cookies.get('access-token')) === '0' || this.state.userID === '-1') {
            return <ErrorPage />
        } else {
            return (
                <div>
                    <div id="profilePicContainer" className="row container-fluid d-flex justify-content-center mt-0" data-toggle="modal" data-target="#editAvatar">

                        <UserAvatar UID={this.state.userID} name={this.state.result.FirstName + " " + this.state.result.LastName} size="20mm" />

                    </div>

                    <div className='d-flex flex-wrap' id="modal-main">
                        <div className="modal fade main-modal" id='editAvatar'>
                            <div className="modal-dialog">
                                <div className="modal-content">
                                    <div className="modal-body">
                                        <div className='px-2'>
                                            <div className="user-settings d-flex mt-3 justify-content-start"  >
                                                <label className='d-flex d-inline-flex'>
                                                    <img className="d-block mr-3 ml-1" src="../../icons/upload.png" alt="upload-avatar" />
                                                    <input type="file" name="file" accept="image/*" className="visually-hidden" onChange={this.submitAvatar} />
                                                    <span className="mr-2 text-white">upload picture</span>
                                                </label>
                                            </div>
                                            {this.state.result.Avatar !== 'NULL' ?
                                                <div className="user-settings d-flex mt-3 justify-content-start">
                                                    <label className='d-flex d-inline-flex' onClick={(event) => this.deleteAvatar(this.state.result.Avatar, event)}>
                                                        <img className="d-block mr-3 ml-1" src="../../icons/remove.png" alt="upload-avatar" />
                                                        <span className="mr-2 text-white">remove avatar</span>
                                                    </label>
                                                </div>
                                                :
                                                <div></div>
                                                }
                                            <div className="text-white d-flex pt-2 mx-auto" data-dismiss="modal">cancel</div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>

                    <span id='username' className="ml-1 font-weight-bold">{this.state.result.Username}</span>

                    <div>
                        <div id='settings' className='flex-column mt-5'>
                            <div className="user-settings d-flex mt-3 justify-content-start" onClick={() => { window.location.href = '/vehicle' }}>
                                <img className="d-block mr-3 ml-1" src="../../icons/tyre.png" alt="edit-address" />
                                <span className="mr-2 text-white">vehicle details</span>
                                <span className='d-flex justify-content-end ml-auto'>〉</span>
                            </div>
                            <div className="user-settings d-flex mt-3 justify-content-start" onClick={() => { window.location.href = '/home' }}>
                                <img className="d-block mr-3 ml-1" src="../../icons/home.png" alt="edit-address" />
                                <span className="mr-2 text-white">home address</span>
                                <span className='d-flex justify-content-end ml-auto'>〉</span>
                            </div>
                            <div className="user-settings d-flex mt-3 justify-content-start" onClick={() => { window.location.href = '/username' }}>
                                <img className="mr-3 ml-1" src="../../icons/snail_id.png" alt="change-username" />
                                <span className="mr-2 text-white">change username</span>
                                <span className='d-flex justify-content-end ml-auto'>〉</span>
                            </div>
                            <div className="user-settings d-flex mt-3 justify-content-start" onClick={() => { window.location.href = '/password' }}>
                                <img className="mr-3 ml-1" src="../../icons/password.png" alt="change-password" />
                                <span className="mr-2 text-white">change password</span>
                                <span className='d-flex justify-content-end ml-auto'>〉</span>
                            </div>
                        </div>
                        <a href="/logout" className='d-flex justify-content-center mt-5 font-weight-normal'><i className="fa fa-sign-out"></i>Logout</a>
                    </div>
                </div>
            );
        }
    }
}

export default UserProfile; 