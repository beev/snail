import React, { Component } from 'react';
import Rating from 'react-rating';

import Cookies from 'js-cookie';

import UserAvatar from './UserAvatar'

import '../css/form.css';

class RateUsers extends Component {

    constructor(props) {
        super(props);
        this.state = {
            ratings: props.ratings,
            rideInfo: props.rideInfo,
            ratingState: "driver",
            loggedInUser: Cookies.get('loggedInUser')
        };
    }

    componentWillMount() {
        console.log(this.state.rideInfo)
        this.state.rideInfo.map((data) => {
            if (data.UID == this.state.loggedInUser && data.driver == 1)
                this.setState({ ratingState: "users" })
            else if (data.UID == this.state.loggedInUser && data.driver != 1 && this.state.rideInfo.length < 3)
                this.setState({ ratingState: "done" })
        })

    }

    ratingChangeHandler(event, user, field) {
        var ratings = this.state.ratings;
        if (field === "text")
            ratings[user.UID][field] = event.target.value;
        else
            ratings[user.UID][field] = event;
        this.setState({ ratings });
    }

    rateSubmitHandler = (event) => {
        event.preventDefault();
        var url;
        if (this.state.ratingState === "driver")
            this.setState({ ratingState: "users" });
        else {
            console.log(this.state.rideInfo)
            for (var uid in this.state.ratings) {
                url = "/ratings/" + uid + "?";
                for (var rating in this.state.ratings[uid]) {
                    if (this.state.ratings[uid][rating])
                        url = url + rating + "=" + this.state.ratings[uid][rating] + "&";
                }

                this.state.rideInfo.map((users) => {
                    if (users.UID == uid && users.driver === 1)
                        url = url + "role=drvr"
                    else if (users.UID == uid && users.driver === 0)
                        url = url + "role=pass"
                }
                )
                fetch(url, { method: 'POST' })
            }
            window.location.pathname = '/posts';

        }
    }

    render() {
        return (
            <form className="form-group container-fluid col-11">
                <h6 className="modal-title text-white small">Rating is anonymous</h6>
                <p className="modal-title text-white small">Just click "submit" to give every user a 5</p>
                <div className="px-0 mx-auto px-auto">
                    {this.state.ratingState === "driver" ?
                        this.state.rideInfo.map((data) =>
                            data.UID != this.state.loggedInUser && data.driver == 1 ?
                                <div className="d-flex flex-column mb-3 mt-3 w-100" key={data.UID}>
                                    <h4 className="modal-title text-white font-weight-normal">Rate the driver</h4>
                                    <div className="d-flex mr-auto mb-0 pb-0 w-auto" >
                                    <UserAvatar UID={data.UID} name={data.FirstName +" "+ data.LastName} size="10mm"/>
                                        <span className='text-truncate d-flex align-self-end ml-1 pt-auto'>
                                            {data.Username}
                                        </span>
                                    </div>
                                    <div className="d-flex flex-column">
                                        <label className='text-secondary small mt-1'><i className="fas fa-dharmachakra mr-2"></i>Driving Skills</label>
                                        <Rating className="pb-1" quiet='false'
                                            initialRating={this.state.ratings[data.UID].DrivingSkills}
                                            id={data.UID}
                                            emptySymbol="far fa-circle fa-2x pr-2 text-white"
                                            fullSymbol="far fa-dot-circle fa-2x pr-2 ratings"
                                            onChange={(event) => this.ratingChangeHandler(event, data, "DrivingSkills")} />
                                        <label className='text-secondary small mt-1'><i className="far fa-clock mr-2"></i>Consistency</label>
                                        <Rating className="pb-1" quiet='false'
                                            initialRating={this.state.ratings[data.UID].ConsistentAsDriver}
                                            id={data.UID}
                                            emptySymbol="far fa-circle fa-2x pr-2 text-white"
                                            fullSymbol="far fa-dot-circle fa-2x pr-2 ratings"
                                            onChange={(event) => this.ratingChangeHandler(event, data, "ConsistentAsDriver")} />
                                        <label className='text-secondary small mt-1'><i className="far fa-laugh mr-2"></i>Behavior</label>
                                        <Rating className="pb-1" quiet='false'
                                            initialRating={this.state.ratings[data.UID].BehaviorAsDriver}
                                            id={data.UID}
                                            emptySymbol="far fa-circle fa-2x pr-2 text-white"
                                            fullSymbol="far fa-dot-circle fa-2x pr-2 ratings"
                                            onChange={(event) => this.ratingChangeHandler(event, data, "BehaviorAsDriver")} />
                                        <label className='text-secondary small mt-1'>Add a comment</label>
                                        <input
                                            id="chatInput"
                                            className="form-control"
                                            placeholder="Write your message"
                                            type='text'
                                            name='no'
                                            value={this.state.ratings[data.UID].text}
                                            onChange={(event) => this.ratingChangeHandler(event, data, "text")}
                                        />
                                    </div>
                                </div>
                                :
                                null)

                        :
                        this.state.ratingState == "users" ?
                        <div>
                            <h4 className="modal-title text-white font-weight-normal">Rate the users:</h4>
                            {this.state.rideInfo.map((data) =>
                                data.UID != this.state.loggedInUser && data.driver == 0 ?
                                    <div className="d-flex flex-column mb-5 mt-3 w-100" key={data.UID}>
                                        <div className="d-flex mr-auto mb-0 pb-0 w-auto" >
                                        <UserAvatar UID={data.UID} name={data.FirstName +" "+ data.LastName} size="10mm"/>
                                            <span className='text-truncate d-flex align-self-end ml-1 pt-auto'>
                                                {data.Username}
                                            </span>
                                        </div>
                                        <div className="d-flex flex-column">
                                            <label className='text-secondary small mt-1'><i className="far fa-clock mr-2"></i>Consistency</label>
                                            <Rating className="pb-1" quiet='false'
                                                initialRating={this.state.ratings[data.UID].Consistent}
                                                id={data.UID}
                                                emptySymbol="far fa-circle fa-2x pr-2 text-white"
                                                fullSymbol="far fa-dot-circle fa-2x pr-2 small ratings"
                                                onChange={(event) => this.ratingChangeHandler(event, data, "Consistent")} />
                                            <label className='text-secondary small mt-1'><i className="far fa-laugh mr-2"></i>Behavior</label>
                                            <Rating className="pb-1" quiet='false'
                                                initialRating={this.state.ratings[data.UID].Behavior}
                                                id={data.UID}
                                                emptySymbol="far fa-circle fa-2x pr-2 text-white"
                                                fullSymbol="far fa-dot-circle fa-2x pr-2 small ratings"
                                                onChange={(event) => this.ratingChangeHandler(event, data, "Behavior")} />
                                            <label className='text-secondary small mt-1'>Add a comment</label>
                                            <input
                                                id="chatInput"
                                                className="form-control"
                                                placeholder="Write your message"
                                                type='text'
                                                name='no'
                                                value={this.state.ratings[data.UID].text}
                                                onChange={(event) => this.ratingChangeHandler(event, data, "text")}
                                            />
                                        </div>
                                    </div>
                                    :
                                    null
                            )}
                        </div>
                        :
                        null
                    }
                </div>
                <div className="d-flex flex-column mx-auto" onClick={this.rateSubmitHandler}>Submit</div>
            </form>
        );
    }
}

export default RateUsers;
