import React from 'react';
import '../css/form.css';

class LoginForm extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            login: '',
            password: '',
            result: []
        };
    }

    changeHandler = (event) => {
        let name = event.target.name;
        let value = event.target.value;
        this.setState({ [name]: value });
    }

    submitHandler = (event) => {
        event.preventDefault();
        let url = "/login/?login=" + this.state.login + "&password=" + this.state.password;
        fetch(url, { method: 'POST' })
            .then(res => res.json())
            .then(data => this.setState({ result: data }))
    }
    
    componentWillUnmount(){
        
    };

    render() {
        let err_mssg = '';
        if (this.state.result.length !== 0 && this.state.result.auth === true)
            return window.location.href = '/posts';
        else if (this.state.result.length !== 0 && this.state.result.auth === false)
            err_mssg = 'Invalid username or password!';
        return (
            <form
                className="form-group container-fluid col-11"
                onSubmit={this.submitHandler}>
                <h2 className='font-weight-normal'>Welcome back!</h2>
                <h5 className='font-weight-normal'>we missed you</h5>
                <label className="small text-danger">{err_mssg}</label>
                <div className="row">
                    <div className="col">
                        <input
                            id="login"
                            className="form-control"
                            placeholder="Username or E-mail"
                            type='text'
                            name='login'
                            onChange={this.changeHandler}
                        />
                    </div>
                </div>
                <div className="row">
                    <div className="col">
                        <input
                            id="pwd"
                            className="form-control"
                            placeholder="Password"
                            type='password'
                            name='password'
                            onChange={this.changeHandler}
                        />
                    </div>
                </div>
                <div className="row center">
                    <div className="col center d-flex justify-content-center">
                        <button
                            type='submit'
                            className="btn btn-dark"
                        >Sign In</button>
                    </div>
                </div>
            </form>
        );
    }
}
export default LoginForm;
