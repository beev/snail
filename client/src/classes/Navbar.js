import React, { Component } from 'react';

import Cookies from 'js-cookie';
import UserAvatar from './UserAvatar';
import '../css/navbar.css';

class Navbar extends Component {

    constructor(props) {
        super(props);
        this.state = {
            result: [],
            userID: '',
            user: []
        };
    }

    componentDidMount() {
        if (this.props.auth === true) {
            this.setState({userID: JSON.parse(Cookies.get('loggedInUser'))});
            fetch('/users/' + this.state.userID)
                .then(res => res.json())
                .then(res => { this.setState({ user: res[0] }); console.log(this.state.user) })
        }
    }

    render() {
        return (
            this.props.auth ?
                <div className="d-flex d-inline-flex w-100">
                    <div id='logoBtn' className=''>
                        <a href="/posts" className=" mr-auto px-0 font-weight-normal text-white">
                            <img src="../icons/logo-white.png" alt="logo" />Snail
                        </a>
                    </div>
                    {this.props.profile ?
                        null
                        :
                        <div id='profilePicture' className='ml-auto mr-0 px-0 pt-2 w-auto'>
                            <a onClick={() => window.location.pathname = "/users/" + this.state.userID} >
                                <UserAvatar UID={this.state.UID} size="8mm" name={this.state.user.FirstName + " " + this.state.user.LastName} />
                            </a>
                        </div>
                    }
                </div>
                :
                this.props.back ?
                    <i id='back' className="w-auto ml-2 mt-2 fas fa-long-arrow-alt-left" onClick={() => window.location.pathname = '/'}></i>
                    :
                    null
        );
    }

}

export default Navbar;
