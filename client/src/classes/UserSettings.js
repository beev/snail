import React from 'react'
import Cookies from 'js-cookie';
import Areas from './Areas';

import '../css/profile.css';
import '../css/index.css';

class UserSettings extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            Username: '',
            Password: '',
            Password2: '',
            Address: '',
            VehicleDetails: '',
            Vehicle: '',
            result: [],
            err_mssg: '',
            mssg: ''
        };
    }

    componentDidMount() {
        let url = "/users/" + JSON.parse(Cookies.get('loggedInUser'));
        fetch(url)
            .then(res => res.json())
            .then(data => this.setState({ result: data[0] }))
    }

    changeHandler = (event) => {
        let name = event.target.name;
        let value = event.target.value;

        this.setState({ [name]: value });

        if (name === 'Vehicle' && value === 'none') {
            event.target.style.display = "none";
            this.setState({ mssg: 'If you have no vehicle you cannot post for giving a ride.' })
        } else {
            event.target.style.display = "block";
            this.setState({ mssg: '' })
        }

        /**check passwords' similarity */
        if (name === 'Password2') {
            if (value !== this.state.Password) {
                event.target.setCustomValidity("Invalid field.");
                this.setState({ err_mssg: 'Passwords don\'t match' });
            } else {
                event.target.setCustomValidity("");
                this.setState({ err_mssg: '' });
            }
        }

        if (!event.target.checkValidity())
            return;
    }

    submitHandler = (event) => {
        event.preventDefault();
        var userID = document.cookie.replace(/(?:(?:^|.*;\s*)loggedInUser\s*\=\s*([^;]*).*$)|^.*$/, "$1");

        if (this.state.Vehicle === '')
            this.state.Vehicle = 'none';

        let url = "/users";

        if (this.props.content === 'vehicle')
            url = url + "?Vehicle=" + this.state.Vehicle;
        else if (this.props.content === 'home')
            url = url + "?Address=" + this.state.Address;
        else if (this.props.content === 'username')
            url = url + "?Username=" + this.state.Username;
        else if (this.props.content === 'password')
            url = url + "?Password=" + this.state.Password;

        if (!event.target.checkValidity())
            return;

        if (this.props.content === 'username') {
            fetch(url, { method: 'PUT' })
                .then(res => res.json())
                .then(data => {
                    if (data.err_mssg)
                        this.setState({ err_mssg: 'This username is taken' });
                    else {
                        this.setState({ err_mssg: '' })
                        window.location.pathname = '/users/' + userID;
                    }
                })
        } else {
            fetch(url, { method: 'PUT' })
                .then(res => res.json())
                .then(window.location.pathname = '/users/' + userID)
        }
    }

    vehicleSettings() {
        return (
            <div className='d-flex justify-content-center flex-column'>
                <form noValidate
                    className="form-group container-fluid col-11"
                    onSubmit={this.submitHandler}>
                    <p className="small">Change your vehicle and it's details</p>
                    <label className="small text-secondary">{this.state.mssg}</label>
                    <div className="row flex-column">
                        <div>
                            <input
                                id='vehicle-details'
                                className="form-control"
                                placeholder="Describe your car (e.g. Brand name, Model, Colour)"
                                type='text'
                                name='vehicleDetails'
                                pattern='.{8,}'
                                value={this.state.result.VehicleDetails}
                                onChange={this.changeHandler}
                            />
                        </div>
                        {/**vehicle type input*/}
                        <div className="col">
                            <label className='small text-white mt-2 mb-0 pb-1'>Vehicle Type</label>
                            <select className="form-control" name='Vehicle' value={this.state.Vehicle} onChange={this.changeHandler}>
                                <option value="car" className='col-sm-'>&#128663;</option>
                                <option value="moto" className='col-sm'>&#128757;</option>
                                <option value="none" className='col-sm'>&#127939;</option>
                            </select>
                        </div>

                        <div className="col center d-flex justify-content-center">
                            <button
                                type='submit'
                                className="btn btn-dark"
                            >Save</button>
                        </div>
                    </div>
                </form>
            </div>
        );
    }

    homeAddress() {
        return (
            <div className='d-flex justify-content-center flex-column'>
                <form noValidate
                    className="form-group container-fluid col-11"
                    onSubmit={this.submitHandler}>
                    <p className="small">Set a new address as Home</p>
                    <div className="row flex-column">
                        {/**home area input*/}
                        <div className="col">
                            <label className='small text-white mt-2 mb-0 pb-1'>Address</label>
                            <Areas name='Address' source={this} />
                        </div>
                    </div>

                    <div className="col center d-flex justify-content-center">
                        <button
                            type='submit'
                            className="btn btn-dark"
                        >Save</button>
                    </div>
                </form>
            </div >
        );
    }

    changeUsername() {
        return (
            <div className='d-flex justify-content-center flex-column'>
                <form noValidate
                    className="form-group container-fluid col-11"
                    onSubmit={this.submitHandler}>
                    <p className="small">Change your username</p>
                    <div className="row flex-column">
                        {/**username input*/}
                        <div className="col">
                            <label className='small text-white mt-2 mb-0 pb-1'>Username (4 to 10 characters)</label>
                                <input
                                    key={this.state.result.UID}
                                    id="username"
                                    className="form-control"
                                    type='text'
                                    name='Username'
                                    pattern='^(?=.{4,10}$)(?![_])(?!.*[_]{2})[a-zA-Z0-9_]+(?<![_])$'
                                    value={this.state.Username}
                                    onChange={this.changeHandler}
                                    placeholder={this.state.result.Username}
                                />

                        </div>
                    </div>
                    <label className="small text-danger">{this.state.err_mssg}</label>
                    <div className="col center d-flex justify-content-center">
                        <button
                            type='submit'
                            className="btn btn-dark"
                        >Save</button>
                    </div>
                </form>
            </div >
        );
    }

    changePassword() {
        return (
            <div className='d-flex justify-content-center flex-column'>
                <form noValidate
                    className="form-group container-fluid col-11"
                    onSubmit={this.submitHandler}>
                    <p className="small">Change your password</p>
                    <div className="row flex-column">
                        <label className="small text-danger">{this.state.err_mssg}</label>
                        {/**password input*/}
                        <div className="col">
                            <label className='small text-white mt-2 mb-0 pb-1'>Password</label>
                            <input
                                id="pwd"
                                className="form-control"
                                type='password'
                                name='Password'
                                pattern='.{8,}'
                                value={this.state.Password}
                                onChange={this.changeHandler}
                                required
                            />
                        </div>

                        {/**password validation input*/}
                        <div className="col">
                            <label className='small text-white mt-2 mb-0 pb-1'>Confirm Password</label>
                            <input
                                id="pwd2"
                                className="form-control"
                                type='password'
                                name='Password2'
                                pattern={this.state.Password}
                                value={this.state.Password2}
                                onChange={this.changeHandler}
                                required
                            />
                        </div>

                        <div className="col center d-flex justify-content-center">
                            <button
                                type='submit'
                                className="btn btn-dark"
                            >Save</button>
                        </div>
                    </div>
                </form>
            </div >
        );
    }


    render() {
        if (JSON.stringify(Cookies.get('access-token')) === '0' || JSON.stringify(Cookies.get('loggedInUser')) === '-1') {
            return <div>Access on user denied</div>
        } else {
            if (this.props.content === 'vehicle')
                return this.vehicleSettings();
            else if (this.props.content === 'home')
                return this.homeAddress();
            else if (this.props.content === 'username')
                return this.changeUsername();
            else if (this.props.content === 'password')
                return this.changePassword();
        }
    }
}
export default UserSettings; 