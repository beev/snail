import React from 'react'
import '../css/index.css';

class HomePage extends React.Component {
    componentDidMount(){
        fetch("/logout", {method: 'POST'})
    }

    render() {
        return (
            <div className='center'>
                <div className="row d-flex justify-content-center mt-0" id="logo">
                    <img src="../../icons/logo-white.png" alt="logo-white" className="center"></img>
                </div>
                <div className="row d-flex flex-column justify-content-center pl-4" id="moto">
                    <h2 className="p-1 font-weight-normal">Hop on</h2>
                    <h5 className="p-1 font-weight-normal">get there faster</h5>
                    <h6 className="p-1 font-weight-normal">make new friends on the way</h6>
                </div>
                <div className="row-md d-flex justify-content-center" id="register">
                    <a href="/register" id="register-btn" type="button" className="btn">Join The Party</a>
                </div>
                <div className="row d-flex flex-row justify-content-center mt-2" id="login">
                    <p>already a member? &nbsp;</p>
                    <a href="/login"> Sign in.</a>
                </div>
                <div className="row d-flex flex-row justify-content-center mt-2" id="howTo">
                    <button onClick={()=>window.location.pathname="/howToUse"} className='btn btn-dark'><i className="fas fa-info pr-2"></i>How to use</button>
                </div>
            </div>
        );
    }
}

export default HomePage; 