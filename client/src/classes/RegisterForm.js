import React from 'react';
import DatePicker from "react-datepicker";
import "react-datepicker/dist/react-datepicker.css";
import Areas from './Areas';
import '../css/form.css';

class RegisterForm extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            Username: '',
            Password: '',
            Password2: '',
            FirstName: '',
            LastName: '',
            Email: '',
            result: [],
            err_mssg: ''
        };
    }


    changeHandler = (event) => {
        let name = event.target.name;
        let value = event.target.value;

        this.setState({ [name]: value });
        this.setState({ err_mssg: '' });

        /**check passwords' similarity */
        if (name === 'Password2') {
            if (value !== this.state.Password) {
                event.target.setCustomValidity("Invalid field.");
                this.setState({ err_mssg: 'Passwords don\'t match' });
            } else {
                event.target.setCustomValidity("");
                this.setState({ err_mssg: '' });
            }
        }

        if (!event.target.checkValidity())
            return;
    }


    submitHandler = (event) => {
        event.preventDefault();

        if (this.state.Vehicle === '')
            this.state.Vehicle = 'none';

        let url = "/users"
            + "?Username=" + this.state.Username
            + "&Password=" + this.state.Password2
            + "&LastName=" + this.state.LastName
            + "&FirstName=" + this.state.FirstName
            + "&Email=" + this.state.Email;

        if (!event.target.checkValidity())
            return;

        fetch(url, { method: 'POST' })
            .then(res => res.json())
            .then(data => this.setState({ result: data }, () => {
                if (data.err_mssg)
                    this.setState({ err_mssg: 'This username is taken' });
                else {
                    let url = "/login/?login=" + this.state.Username + "&password=" + this.state.Password2;
                    fetch(url, { method: 'POST' })
                        .then(res => res.json())
                        .then(data => {
                            if (data.length !== 0 && data.auth === true)
                                window.location.href = '/posts'
                        }
                        )
                }
            }))
    }

    render() {
        return (
            <form noValidate
                className="form-group container-fluid col-11"
                onSubmit={this.submitHandler}>
                <p className="small">We promise you won't regret this</p>
                <div className="row flex-column">
                    {/**email input*/}
                    <div className="col">
                        <label className='small text-white mt-2 mb-0 pb-1'>E-mail</label>
                        <input
                            id="Email"
                            className="form-control"
                            type='text'
                            name='Email'
                            pattern='^[^@]+@[^@]+\.[^@]+$'
                            value={this.state.Email}
                            onChange={this.changeHandler}
                            required
                        />
                    </div>

                    {/**first name input*/}
                    <div className="col">
                        <label className='small text-white mt-2 mb-0 pb-1'>First Name</label>
                        <input
                            id="firstName"
                            className="form-control"
                            type='text'
                            name='FirstName'
                            pattern='^(?=.{2,20}$)(?![_])(?!.*[_]{2})[a-zA-Z_]+(?<![_])$'
                            value={this.state.FirstName}
                            onChange={this.changeHandler}
                            required
                        />
                    </div>

                    {/**last name input*/}
                    <div className="col">
                        <label className='small text-white mt-2 mb-0 pb-1'>Last Name</label>
                        <input
                            id="lastName"
                            className="form-control"
                            type='text'
                            name='LastName'
                            pattern='^(?=.{2,20}$)(?![_])(?!.*[_]{2})[a-zA-Z_]+(?<![_])$'
                            value={this.state.LastName}
                            onChange={this.changeHandler}
                            required
                        />
                    </div>

                    {/**username input*/}
                    <div className="col">
                        <label className='small text-white mt-2 mb-0 pb-1'>Username (4 to 10 characters)</label>
                        <input
                            id="username"
                            className="form-control"
                            type='text'
                            name='Username'
                            pattern='^(?=.{4,10}$)(?![_])(?!.*[_]{2})[a-zA-Z0-9_]+(?<![_])$'
                            value={this.state.Username}
                            onChange={this.changeHandler}
                            required
                        />
                    </div>

                    {/**password input*/}
                    <div className="col">
                        <label className='small text-white mt-2 mb-0 pb-1'>Password (at least 8 characters)</label>
                        <input
                            id="pwd"
                            className="form-control"
                            type='password'
                            name='Password'
                            pattern='.{8,}'
                            value={this.state.Password}
                            onChange={this.changeHandler}
                            required
                        />
                    </div>

                    {/**password validation input*/}
                    <div className="col">
                        <label className='small text-white mt-2 mb-0 pb-1'>Confirm Password</label>
                        <input
                            id="pwd2"
                            className="form-control"
                            type='password'
                            name='Password2'
                            pattern={this.state.Password}
                            value={this.state.Password2}
                            onChange={this.changeHandler}
                            required
                        />
                        <label className="small text-danger">{this.state.err_mssg}</label>
                    </div>

                    <div className="col center d-flex justify-content-center">
                        <button
                            type='submit'
                            className="btn btn-dark"
                        >Register</button>
                    </div>
                </div>
            </form>
        );
    }
}
export default RegisterForm;
