import React, { Component } from 'react';
import '../css/form.css';

class Areas extends Component {

  constructor(props) {
    super(props);
    this.state = { result: [] };
  }

  componentWillMount() {
    fetch("/areas")
      .then(res => res.json())
      .then(data => this.setState({ result: data }))
  }

  render() {
    if (this.state.result) {
      return (
        <div>
          <input className="form-control" list="areas" id="areasList" name={this.props.name} onChange={this.props.source.changeHandler}/>
          <datalist id='areas'>
            {this.state.result.map((data) =>
              <option value={data.Name} key={data.AID}>
                {data.Name}
              </option>
            )}
          </datalist>
        </div>
      );
    } else {
      return (
        <div className="Areas"></div>
      );
    }
  }
}

export default Areas;
