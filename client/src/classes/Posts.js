import React from 'react';
import Cookies from 'js-cookie';
import TimeAgo from 'react-timeago';
import '../css/Posts.css';
import Areas from './Areas';
import ViewRatings from './ViewRatings';
import UserAvatar from './UserAvatar';

class Posts extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      StartingArea: '',
      DestinationArea: '',
      NumberOfParticipants: '1',
      NumberOfAvailableSeats: '4',
      joinAs: 'driver',
      resultShell: [],
      resultSlug: [],
      resultHomePage: [],
      result: [],
      loggedInUser: Cookies.get('loggedInUser'),
      gettingOn: 0,
      err_mssg: '',
      avatar: {},
      rid: '-1'
    };
  }

  componentDidMount() {
    if (this.props.main === 'shell') { //fetch shell posts
      fetch('/postsShell')
        .then(res => res.json())
        .then(data => { this.setState({ resultShell: data }); console.log(data); return data; })
    } else if (this.props.main === 'slug') { //fetch slug posts
      fetch('/postsSlug')
        .then(res => res.json())
        .then(data => { this.setState({ resultSlug: data }); return data; })
    } else { //fetch ride user is on, if exists
      fetch('/rides/user/' + this.state.loggedInUser)
        .then(res => res.json())
        .then(data => { this.setState({ resultHomePage: data }); console.log(this.state.resultHomePage) })
    }
  }

  changeHandler = (event) => {
    let name = event.target.name;
    let value = event.target.value;

    this.setState({ err_mssg: '' })
    this.setState({ [name]: value });
  }

  submitHandler = (type, event) => {
    event.preventDefault();

    /**check if all fields are filled */
    if (this.state.StartingArea === '' || this.state.DestinationArea === '') {
      this.setState({ err_mssg: 'please fill all fields' })
      return;
    }

    console.log("type = " + type);

    /**URL query */
    let url = "?start=" + this.state.StartingArea + "&destination=" + this.state.DestinationArea;
    if (type === 'Shell')
      url = "/postsShell" + url
    else if (type === 'Slug')
      url = "/postsSlug" + url + "&seats=" + this.state.NumberOfAvailableSeats;
    fetch(url, { method: 'POST' })
      .then(res => res.json())
      .then(data => this.setState({ result: data }))
      .then(window.location.href = '/posts' + type)
  }

  deletePost = (type, data, event) => {
    event.preventDefault();
    var url;
    if (type === 'Shell')
      url = "/postsShell/" + data.PID;
    else if (type === 'Slug')
      url = "/postsSlug/" + data.PID;

    fetch(url, { method: 'DELETE' })
      .then(res => res.json())
      .then(dt => this.setState({ result: dt }))
      .then(window.location.href = '/posts' + type)
  }

  joinRide = (type, data, event) => {
    event.preventDefault();
    var rideURL = "/rides?PID=" + data.PID + "&type=" + type + "&UID=" + this.state.loggedInUser;

    if (type === 'Slug') { /**update Slug post */
      if (this.state.NumberOfAvailableSeats > 0) {/** number of available seats */
        var updateURL = '/postsSlug/' + data.PID + '?seats=' + (+data.NumberOfAvailableSeats - 1 + '');
        fetch(updateURL, { method: 'PUT' })
          .then(
            fetch(rideURL, { method: 'POST' })
              .then(res => res.json())
              .then(dt => {
                this.setState({ result: dt });
                if (this.state.result.rid && dt.affectedRows != 0)
                  window.location.href = '/rides/' + this.state.result.rid;
              }
              )
          )
      }
    } else if (type === 'Shell') {/**update Shell post */
      var updateURL = '/postsShell/' + data.PID + '?passengers=' + (+data.NumberOfParticipants + 1 + '');

      if (this.state.joinAs === 'driver' && data.NumberOfParticipants < 4) {/**if a driver joined this post */
        updateURL = updateURL + "&driver=1";
      } else { /**if another participant joins a shell post */
        if (data.NumberOfParticipants < 5) { /**MAX of 4 passengers per post */
          updateURL = updateURL + "&driver=0";
        } else
          return;
      }
      fetch(updateURL, { method: 'PUT' })
        .then(res => res.json())
        .then(res => {
          if (res.affectedRows == 0) { this.setState({ err_mssg: 'this post already has a driver' }); return; }
          this.setState({ err_mssg: '' });
          fetch(rideURL, { method: 'POST' })
            .then(res => res.json())
            .then(dt => {
              this.setState({ result: dt });
              if (this.state.result.rid)
                window.location.href = '/rides/' + this.state.result.rid;
            }
            )
        })
    }


  }

  bottomElements() {
    var shell, slug, home;
    /**bottom images selection */
    if (this.props.main === 'shell') {
      shell = <img className="menu-icon" src="../../icons/shell-focus.png" alt="get ride" />;
      slug = <img className="menu-icon" src="../../icons/slug.png" alt="give ride" />;
      home = <img className="menu-icon" src="../../icons/snail-new-post.png" alt="new" />;
    } else if (this.props.main === 'slug') {
      shell = <img className="menu-icon" src="../../icons/shell.png" alt="get ride" />;
      slug = <img className="menu-icon" src="../../icons/slug-focus.png" alt="slug" />;
      home = <img className="menu-icon" src="../../icons/snail-new-post.png" alt="new" />;
    } else {
      shell = <img className="menu-icon" src="../../icons/shell.png" alt="get ride" />;
      slug = <img className="menu-icon" src="../../icons/slug.png" alt="give ride" />;
      home = <img className="menu-icon" src="../../icons/snail-new-post-focus.png" alt="all-posts" />;
    }

    return (
      <div className="nav d-flex flex-column fixed-bottom justify-content-end">
        {/**bottom navbar */}
        <div className="row d-flex d-inline-flex justify-content-between">
          <div className="mx-auto align-self-center" id='newPost'>
            <a href='/posts'>{home}</a>
          </div>
          <div className="mx-auto align-self-center" id='slugPosts'>
            <a href='/postsSlug'>{slug}</a>
          </div>
          <div className="mx-auto align-self-center" id='shellPosts'>
            <a href='/postsShell'>{shell}</a>
          </div>
        </div>
      </div>
    );
  }

  getBackToRide() {
    return (
      <div>
        <div className="split container-fluid">
        </div>
        <div className="d-flex flex-column">
          <div className="container-fluid d-flex d-inline-flex justify-content-between mt-5"></div>
          <button type="button" className="btn-new p-1 mr-2 mt-5" onClick={() => window.location.href = "/rides/" + this.state.resultHomePage.rid}>
            <img className="mx-auto w-50 btn-new-img p-3 shadow-lg" src="../../icons/ride.png" alt="ride" />
            <p className='new-btn-txt font-weight-normal mx-auto mb-0 p-2'>My ride</p>
          </button>
        </div>
      </div>
    )
  }

  ActivePost() {
    return (
      <div>
        <div className="split container-fluid">
        </div>
        <div className="d-flex flex-column">
          <div className="font-weight-bolder mt-5">
            <h2>You already have an active post!</h2>
            <h5>Just wait for more people to join</h5>
          </div>
        </div>
      </div>
    )
  }

  createNewPost() {
    return (
      <div>
        <div className="split container-fluid">
        </div>
        <div className="d-flex flex-column">
          <h4 className="font-weight-normal">Create a new ride</h4>
          <div className="container-fluid d-flex d-inline-flex justify-content-between mt-5">
            {/**create new slug post */}
            <button type="button" className="btn-new p-1 mr-2 mt-5" data-toggle="modal" data-target="#createNewSlug">
              <img className="mx-auto w-75 btn-new-img p-3 shadow-lg" src="../../icons/slug.png" alt="slug" />
              <p className='new-btn-txt font-weight-normal mx-auto mb-0 p-2'>Give a ride</p>
            </button>
            {/**create new shell post */}
            <button type="button" className="btn-new p-1 ml-2 mt-5" data-toggle="modal" data-target="#createNewShell">
              <img className="mx-auto w-75 btn-new-img p-3 shadow-lg" src="../../icons/shell.png" alt="shell" />
              <p className='new-btn-txt font-weight-normal mx-auto mb-0 p-2'>Get a ride</p>
            </button>
          </div>
        </div>
        {/**new slug pop-up */}
        <div className='d-flex flex-wrap' id="modal-main">
          <div className="modal fade main-modal pt-0 h-100" id="createNewSlug">
            <div className="modal-dialog">
              <div className="modal-content h-auto">

                <div className="modal-header">
                  <div className="d-flex flex-column">
                    <h4 className="modal-title font-weight-normal">Create a new post</h4>
                    <h6>Give a ride</h6>
                  </div>
                  <button type="button" className="close text-white" data-dismiss="modal">&times;</button>
                </div>

                <div className="modal-body py-0 mx-1 d-block ">
                  <label className="small text-danger">{this.state.err_mssg}</label>
                  <form
                    className="form-group mx-0 my-0 py-2"
                    onSubmit={(event) => this.submitHandler('Slug', event)}>
                    <label className="small text-danger hidden">{/**{err_mssg}*/}</label>
                    {/**starting area selection */}
                    <div className="row">
                      <div className="col">
                        <label className='small text-secondary mt-2 mb-0 pb-1'>Starting From</label>
                        <Areas name='StartingArea' source={this} />
                      </div>
                    </div>

                    {/**destination area selection */}
                    <div className="row">
                      <div className="col">
                        <label className='small text-secondary mt-2 mb-0 pb-1'>Going To</label>
                        <Areas name='DestinationArea' source={this} />
                      </div>
                    </div>
                    {/**number of available seats selection */}
                    <div className="col">
                      <label className='small text-secondary mt-2 mb-0 pb-1'>number of available seats</label>
                      <div className="d-flex justify-content-between mx-auto" >
                        <i className="fas fa-minus number-input-contrl p-2" onClick={() => { if (this.state.NumberOfAvailableSeats > 1) this.setState({ NumberOfAvailableSeats: +this.state.NumberOfAvailableSeats - 1 + '' }) }}></i>
                        <input type="number" className="ml-3 my-auto" id="seats" name="NumberOfAvailableSeats" min='1' max='4' value={this.state.NumberOfAvailableSeats} onChange={this.changeHandler} />
                        <i className="fas fa-plus number-input-contrl p-2" onClick={() => { if (this.state.NumberOfAvailableSeats < 4) this.setState({ NumberOfAvailableSeats: +this.state.NumberOfAvailableSeats + 1 + '' }) }}></i>
                      </div>
                    </div>
                    <div className="row center">
                      <div className="col center d-flex justify-content-center">
                        <button
                          type='submit'
                          className="btn btn-dark"
                        >Post</button>
                      </div>
                    </div>
                  </form>
                </div>
              </div>
            </div>
          </div>
        </div>
        {/**new shell pop-up */}
        <div className='d-flex flex-wrap' id="modal-main">
          <div className="modal fade main-modal h-100" id="createNewShell">
            <div className="modal-dialog">
              <div className="modal-content h-auto">

                <div className="modal-header">
                  <div className="d-flex flex-column">
                    <h4 className="modal-title font-weight-normal">Create a new post</h4>
                    <h6>Get a ride</h6>
                  </div>
                  <button type="button" className="close text-white" data-dismiss="modal">&times;</button>
                </div>

                <div className="modal-body py-0 mx-1 d-block ">
                  <label className="small text-danger">{this.state.err_mssg}</label>
                  <form
                    className="form-group mx-auto my-0 py-2"
                    onSubmit={(event) => this.submitHandler('Shell', event)}>
                    <label className="small text-danger hidden">{/**{err_mssg}*/}</label>
                    {/**starting area selection */}
                    <div className="row">
                      <div className="col">
                        <label className='small text-secondary mt-2 mb-0 pb-1'>Starting From</label>
                        <Areas name='StartingArea' source={this} />
                      </div>
                    </div>

                    {/**destination area selection */}
                    <div className="row">
                      <div className="col">
                        <label className='small text-secondary mt-2 mb-0 pb-1'>Going To</label>
                        <Areas name='DestinationArea' source={this} />
                      </div>
                    </div>
                    <div className="row center">
                      <div className="col center d-flex justify-content-center">
                        <button
                          type='submit'
                          className="btn btn-dark"
                        >Post</button>
                      </div>
                    </div>
                  </form>
                </div>
              </div>

            </div>
          </div>
        </div>
      </div>
    );
  }

  /**displaying slug posts */
  displaySlugPosts() {
    var i = 0;
    return (
      <div className="Posts pb-5">
        <p className='mb-1'>People giving a ride</p>
        {this.state.resultSlug.map((data) =>
          <div id={"post" + i} key={i++}>
            <div className="post pt-2 px-2 pb-1 mb-2 slug">
              {/**basic user's info */}
              <div className='d-flex justify-content-between'>
                <UserAvatar UID={data.UID} name={data.FirstName + " " + data.LastName} size="8mm" />
                <p className='mr-auto mt-2' data-toggle="modal" data-target={"#viewUser" + data.UID}>@{data.Username}</p>
                <img className="menu-icon h-50 w-10 ml-auto" src="../../icons/slug-focus.png" alt="slug" />
              </div>
              {/**post's info */}
              <div className="d-flex flex-column pl-1">
                <span>{data.StartingArea}<i className='pl-1 pr-1 fas fa-long-arrow-alt-right'></i>{data.DestinationArea}</span>
                <span>
                  <p className="mb-0"><small>room for</small></p>
                  <p className="mt-0"><i className='fas fa-users pr-1'></i>{data.NumberOfAvailableSeats}</p>
                </span>
              </div>
              <div className='d-flex align-content-end mb-2 px-1'>
                <small className='mr-auto my-auto'>Posted <TimeAgo date={data.DatePosted} /></small>
                {/**delete option OR join */}
                {this.postBottomButton(data, i, 'Slug')}
              </div>
            </div>

            {/**user's info pop-up */}
            {<ViewRatings data={data} />}
          </div>
        )}
        <p className='mx-auto px-auto pt-3 d-block'><small>That's all.</small></p>
      </div>
    )
  }

  /**displaying shell posts */
  displayShellPosts() {
    var i = 0;
    return (
      <div className="Posts pb-5">
        <p className='mb-1'>People asking for a ride</p>
        {this.state.resultShell.map((data) =>
          <div id={"post" + i} key={i++}>
            <div className="post pt-2 px-2 pb-1 mb-2 shell">
              {/**basic user's info */}
              <div className='d-flex justify-content-between'>
                <UserAvatar UID={data.UID} name={data.FirstName + " " + data.LastName} size="8mm" />
                <p className='mr-auto mt-2' data-toggle="modal" data-target={"#viewUser" + data.UID}>@{data.Username}</p>
                <img className="menu-icon h-50 w-10 ml-auto" src="../../icons/shell-focus.png" alt="shell" />
              </div>
              {/**post's info */}
              <div className="d-flex flex-column pl-1">
                <span>{data.StartingArea}<i className='pl-1 pr-1 fas fa-long-arrow-alt-right'></i>{data.DestinationArea}</span>
                <span><p><i className='fas fa-users pr-1'></i>{data.NumberOfParticipants}</p></span>
              </div>
              <div className='d-flex align-content-end mb-2 px-1'>
                <small className='mr-auto my-auto'>Posted <TimeAgo date={data.DatePosted} /></small>
                {/**delete option OR join */}
                {this.postBottomButton(data, i, 'Shell')}
              </div>
            </div>

            {/**user's info pop-up */}
            {<ViewRatings data={data} />}
          </div>
        )}
        <p className='mx-auto px-auto pt-3 d-block'><small>That's all.</small></p>
      </div>
    )
  }

  //"delete" or "join" button on each post
  //uid = post's User ID and pid = post's ID
  postBottomButton(data, i, type) {
    var user;
    if (type === 'Shell')
      user = data.UID
    else if (type === 'Slug')
      user = data.UID;

    if (this.state.loggedInUser == user)
      //delete post
      return (
        <div>
          <div className='ml-auto' type="button" data-toggle="modal" data-target={"#deletePost" + i}>
            <i className='far fa-trash-alt pr-1'></i>
          </div>

          {/**deletion verification pop-up */}
          <div className='d-flex flex-wrap' id="modal-main">
            <div className="modal fade main-modal" id={"deletePost" + i}>
              <div className="modal-dialog">
                <div className="modal-content">

                  <div className="modal-header">
                    <h4 className="modal-title text-white font-weight-normal">Are you sure you want to delete this post?</h4>
                  </div>
                  <div className="modal-footer d-flex d-inline-flex">
                    <div type="button" className="btn btn-bg w-25 ml-auto mr-0" onClick={(event) => this.deletePost(type, data, event)}>YES</div>
                    <div type="button" className="btn w-25 ml-2 text-white mr-0" data-dismiss="modal">NO</div>
                  </div>

                </div>
              </div>
            </div>
          </div>

        </div>
      );
    else
      //join ride
      return (
        <div>
          <a href="#" id="join" type="button" className="btn-sm ml-auto mt-0" data-toggle="modal" data-target={"#joinRide" + i}>
            Join <i className='fas fa-caret-right'></i>
          </a>

          {/**ride verification */}
          <div className='d-flex flex-wrap' id="modal-main">
            <div className="modal fade main-modal" id={"joinRide" + i}>
              <div className="modal-dialog">
                <div className="modal-content">

                  <div className="modal-header">
                    <h4 className="modal-title text-white font-weight-normal">You are about to join this ride</h4>
                  </div>
                  {type === 'Shell' ?
                    <div className="modal-body">
                      <label htmlFor="joinAs">Join as:</label>
                      <select className="form-control mx-auto pt-1 px-2" id="join" name="joinAs" onChange={this.changeHandler}>
                        <option className="mx-auto" value="driver">driver</option>
                        {data.NumberOfParticipants > 4 ?
                          <option className="mx-auto text-muted" value="passenger" disabled>passenger</option> :
                          <option className="mx-auto" value="passenger">passenger</option>}
                      </select>
                    </div> : <div></div>}


                  <label className="small text-danger">{this.state.err_mssg}</label>
                  <div className="modal-footer d-flex d-inline-flex">
                    <div type="button" className="btn btn-bg w-25 ml-auto mr-0 " onClick={(event) => this.joinRide(type, data, event)}>Get on</div>
                    <div type="button" className="btn w-25 ml-2 mr-0 text-white" data-dismiss="modal">Cancel</div>
                  </div>

                </div>
              </div>
            </div>
          </div>

        </div>

      );
  }

  mainContent() {
    if (this.props.main === 'new') {
      if (this.state.resultHomePage.rid !== null)
        return this.getBackToRide();
      if (this.state.resultHomePage.pid !== null)
        return this.ActivePost();
      return this.createNewPost()
    } else if (this.props.main === 'shell')
      return this.displayShellPosts()
    else if (this.props.main === 'slug')
      return this.displaySlugPosts()
  }



  render() {
    if (JSON.stringify(Cookies.get('access-token')) === '0' && this.state.loggedInUser === -1) {
      return (
        <div className="Posts">Access on posts denied</div>
      );
    } else {
      if (this.props.main === 'shell' && this.state.resultShell.length === 0) {
        return (
          <div className="Posts">
            <div id='main-container' className="d-flex flex-column mt-5">
              <p className="mt-5">Ooops, nobody needs a ride right now.</p>
              <img src="../../icons/error.png" alt="error" className="mx-auto w-25 mt-3 mb-3"></img>
              <button className="btn btn-dark mx-auto" onClick={() => window.location.pathname = '/posts'}>make a new post</button>
            </div>
            {/**bottom elements*/}
            {this.bottomElements()}
          </div>
        );
      } else if (this.props.main === 'slug' && this.state.resultSlug.length === 0) {
        return (
          <div className="Posts">
            <div id='main-container' className="d-flex flex-column mt-5">
              <p className="mt-5">Ooops, nobody's giving a ride right now.</p>
              <img src="../../icons/error.png" alt="error" className="mx-auto w-25 mt-3 mb-3"></img>
              <button className="btn btn-dark mx-auto" onClick={() => window.location.pathname = '/posts'}>make a new post</button>
            </div>
            {/**bottom elements*/}
            {this.bottomElements()}
          </div>
        );
      } else {
        var err_mssg = 'Please insert both starting area and destination';
        return (
          <div className="Posts mt-4">
            {/**main */}
            {this.mainContent()}

            {/**bottom elements*/}
            {this.bottomElements()}

          </div >
        );
      }
    }
  }

}
export default Posts;