import React, { Component } from 'react';
import ReactDOM from 'react-dom';
import Cookies from 'js-cookie';
import Chat from './Chat';
import UserAvatar from './UserAvatar'

import '../css/chat.css';
import "../css/Ride.css";

// RCE CSS
import 'react-chat-elements/dist/main.css';
// MessageBox component


import '../css/index.css';
import '../css/map.css';
import RateUsers from './RateUsers';
import ErrorPage from './ErrorPage';
import ViewRatings from './ViewRatings';

class Rides extends Component {

    constructor(props) {
        super(props);
        this.state = {
            rideInfo: [],
            loggedInUser: Cookies.get('loggedInUser'),
            loggedInUsername: '',
            startingArea: '',
            destination: '',
            overallRating: 5
        };
    }

    componentWillMount() {
        fetch("/rides/" + this.props.rid)
            .then(res => res.json())
            .then(async dt => {
                this.setState({ rideInfo: dt });
                var ratings = {};
                await Promise.all(dt.map((data) => {
                    if (!this.state.startingArea || !this.state.destination)
                        this.setState({ startingArea: data.StartingArea, destination: data.DestinationArea });
                    if (data.driver == 1) {
                        /**initiate driver's ratings on state */
                        ratings[data.UID] = {
                            "Consistent": null,
                            "Behavior": null,
                            "DrivingSkills": 5,
                            "ConsistentAsDriver": 5,
                            "BehaviorAsDriver": 5,
                            "text": ''
                        }
                    } else {
                        {/**initiate passengers' ratings on state */ }
                        ratings[data.UID] = {
                            "Consistent": 5,
                            "Behavior": 5,
                            "DrivingSkills": null,
                            "ConsistentAsDriver": null,
                            "BehaviorAsDriver": null,
                            "text": ''
                        }
                    }
                    if (data.UID == this.state.loggedInUser)
                        this.setState({ loggedInUsername: data.Username })
                }))
                    .then(this.setState({ ratings }))
            })
    }

    updateRide(action, event) {
        event.preventDefault();
        var url, user = Cookies.get('loggedInUser');

        if (action === 'complete')
            url = "/rides/" + this.props.rid + '?completed=1';
        else if (action === 'leave')
            url = "/rides/" + this.props.rid + '?leftUID=' + user;
        fetch(url, { method: 'PUT' })
            .then(ReactDOM.render(<RateUsers ratings={this.state.ratings} rideInfo={this.state.rideInfo} />, document.getElementById('main')));

    }
    completeRide() {
        var i = 0;
        return (
            <div className='d-flex flex-wrap' id="modal-main">
                <div className="modal fade main-modal" id="complete-ride">
                    <div className="modal-dialog">
                        <div className="modal-content">

                            <div className="modal-header">
                                <h4 className="modal-title text-white font-weight-normal">This ride is over.</h4>
                            </div>
                            <div className="modal-footer d-flex d-inline-flex">
                                <div type="button" className="btn btn-bg w-25 ml-auto mr-0" onClick={(event) => this.updateRide('complete', event)} data-dismiss="modal">Confirm</div>
                                <div type="button" className="btn w-25 ml-2 text-white mr-0" data-dismiss="modal">Cancel</div>
                            </div>

                        </div>
                    </div>
                </div>
            </div>
        )
    }

    leaveRide() {
        return (
            <div className='d-flex flex-wrap' id="modal-main">
                <div className="modal fade main-modal" id="leave-ride">
                    <div className="modal-dialog">
                        <div className="modal-content">

                            <div className="modal-header">
                                <h4 className="modal-title text-white font-weight-normal">Are you sure?</h4>
                            </div>
                            <div className="modal-body">
                                <p>Once you leave you cannot come back!</p>
                                <p>If you're the driver, the whole ride will be deleted.</p>
                            </div>

                            <div className="modal-footer d-flex d-inline-flex">
                                <div type="button" className="btn btn-bg w-25 ml-auto mr-0" onClick={(event) => this.updateRide('leave', event)} data-dismiss="modal">Leave</div>
                                <div type="button" className="btn w-25 ml-2 text-white mr-0" data-dismiss="modal">Stay</div>
                            </div>

                        </div>
                    </div>
                </div>
            </div >
        )
    }

    render() {
        return (
            <div>
                {/**ride's participants */}
                <div id='participants' className='d-flex d-inline-flex mx-auto mt-0 pt-2 w-100'>
                    {this.state.rideInfo.map((data) =>
                        data.UID == this.state.loggedInUser ? <div key={data.UID}></div> :
                            data.driver == 0 ?
                                <div className="d-flex flex-column user-card mr-1 px-2" key={data.UID} data-toggle="modal" data-target={"#viewUser" + data.UID}>
                                    <UserAvatar UID={data.UID} name={data.FirstName + " " + data.LastName} size="10mm" />
                                    <span className='d-inline-block text-truncate '>
                                        <p className='mx-auto'>{data.Username}</p>
                                    </span>
                                    <ViewRatings data={data} />
                                </div>
                                :
                                <div className="d-flex flex-column user-card mr-1 px-2" key={data.UID} data-toggle="modal" data-target={"#viewUser" + data.UID}>
                                    <UserAvatar UID={data.UID} name={data.FirstName + data.LastName} size="10mm" />
                                    <span className='d-inline-block text-truncate '>
                                        <p className='mx-auto'><i className="fas fa-car mr-2"></i>{data.Username}</p>
                                    </span>
                                    <ViewRatings data={data} driver={true} />
                                </div>
                    )}
                </div>

                {/**chat */}
                <button className='btn w-100 complete-ride mx-auto mt-1' data-toggle="modal" data-target="#Chat">
                    <i className="far fa-comment mr-3"></i>message
                    </button>

                {/**message for the participants */}
                <div className='d-flex flex-column mx-auto tip-card mt-3 pt-2 px-2 pb-0'>
                    <i className="fas fa-coins" id="coins"></i>
                    <h5 className='font-weight-normal mt-2 mb-0'>Don't forget to tip the driver!</h5>
                    <p className='font-weight-normal mt-2 mb-0'> It's only fair. It's up to you.</p>
                    <img src="../../icons/snail_profile.png" alt="Avatar" className="w-25 ml-auto" />
                </div>

                {/**ride's info */}
                <div className='d-flex flex-column mx-auto post-card mt-3 mb-3 pt-2 px-2 pb-3'>
                    <i className="fas fa-route post-card-text pl-1"></i>

                    <p className='mx-auto mb-0 pb-0'>from:</p>
                    <h3 className='mx-auto mt-0 pt-0 mb-2 post-card-text'>{this.state.startingArea}</h3>

                    <p className='mx-auto mb-0 pb-0 mt-2'>to:</p>
                    <h3 className='mx-auto mt-0 pt-0 post-card-text'>{this.state.destination}</h3>
                </div>

                {<Chat user={this.state.loggedInUsername} rid={this.props.rid} />}

                {/**end of the ride */}
                <div className="d-flex flex-column mt-0 mb-2 px-2">
                    <button className='btn w-100 complete-ride mx-auto' data-toggle="modal" data-target='#complete-ride'>
                        <i className="fas fa-check mr-3"></i>ride completed
                    </button>
                    <button className="btn btn-dark mx-auto w-100 leave-ride mt-3" data-toggle="modal" data-target='#leave-ride'>
                        <i className="fas fa-door-open mr-3"></i>leave ride
                    </button>
                    {this.completeRide()}
                    {this.leaveRide()}
                </div>
            </div>
        );
    }
}

export default Rides;
