import React, { Component } from 'react';
import { w3cwebsocket as W3CWebSocket } from "websocket";
import { UncontrolledTooltip } from 'reactstrap';
import { Input, Button, MessageBox } from 'react-chat-elements';

const client = new W3CWebSocket('ws://localhost:8000');

class Chat extends Component {
  constructor(props) {
    super(props);
    this.state = {
      messages: [],
      text: '',
      username: this.props.user
    };
  }


  changeHandler(event) {
    this.setState({ text: event.target.value });
  }

  submitHandler(event) {
    event.preventDefault();
    if (this.state.text == "")
      return;
    client.send(JSON.stringify({
      Username: this.props.user,
      RID: this.props.rid,
      Message: this.state.text,
      type: "messagesent"
    }));
    this.setState({ text: '' })
  }

  componentWillMount() {
    client.onopen = () => {
      client.send(JSON.stringify({
        Username: this.props.user,
        RID: this.props.rid,
        type: "userlogin"
      }));
    }

    client.onmessage = (message) => {
      var messages = this.state.messages;
      const dataFromServer = JSON.parse(message.data);
      messages.push(dataFromServer);
      this.setState({ messages })
    };
  }



  chatModal() {
    var i = 0;
    return (
      <div className='d-flex flex-column'>
        <div className="modal fade" id="Chat">
          <div className="modal-dialog h-100">
            <div className="modal-content h-100">
              <div className="modal-header p-1">
                <button type="button" className="close text-white pb-2" data-dismiss="modal">
                  <i className="fas fa-minus"></i>
                </button>
              </div>
              <div className="modal-body px-1 pb-5 pt-2">
                {this.state.messages.map((mssg) =>
                  mssg.Username !== this.props.user ?
                    <div key={i++} className="d-flex flex-column pt-2">
                      <label className="d-flex mr-auto mb-0 text-secondary small">
                        {mssg.Username}
                      </label>
                      <MessageBox
                        className='mr-auto ml-0'
                        position={'left'}
                        type={'text'}
                        titleColor={'Black'}
                        text={mssg.Message}
                      />
                    </div>
                    :
                    <div key={i++} className="d-flex flex-column">
                      <label className="d-flex ml-auto mb-0  text-secondary small">
                        {mssg.Username}
                      </label>
                      <MessageBox
                        className='ml-auto mr-0'
                        position={'right'}
                        type={'text'}
                        titleColor={'Black'}
                        text={mssg.Message}
                      />
                    </div>
                )
                }
              </div>
              <div className="modal-footer d-flex flex-column mx-auto m-0 p-1">
                <form onSubmit={(event) => this.submitHandler(event)} className='d-flex d-inline-flex w-100 px-1 pt-0 pb-0 m-0'>
                  <input
                    id="chatInput"
                    className="form-control"
                    placeholder="Write your message"
                    type='text'
                    name='no'
                    value={this.state.text}
                    onChange={(event) => this.changeHandler(event)}
                  />
                  <button
                    type='submit'
                    className="btn border-0 ml-auto form-control m-0 p-0 w-auto far fa-paper-plane"
                  ></button>
                </form>
              </div>
            </div>
          </div>
        </div>
      </div>
    );
  }

  render() {
    return (
      <div className="container-fluid">
        {this.chatModal()}
      </div>
    );
  }
}

export default Chat;