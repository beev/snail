import React, { Component } from 'react';
import UserAvatar from './UserAvatar';

class ViewRatings extends Component {

    constructor(props) {
        super(props);
        this.state = {
            detailedRatings: [],
            avgRatings: {}
        };
    }

    componentDidMount() {
        var url = "/ratings/" + this.props.data.UID;
        fetch(url)
            .then(res => res.json())
            .then(data => { this.setState({ detailedRatings: data[1], avgRatings: data[0][0] }); console.log(data) })
    }

    //ratings' icons
    rating(rate) {
        if (rate >= 1 && rate < 2)
            rate = 1;
        else if (rate >= 2 && rate < 3)
            rate = 2;
        else if (rate >= 3 && rate < 4)
            rate = 3;
        else if (rate >= 4 && rate < 5)
            rate = 4;
        else if (rate = 5)
            rate = 5;
        else
            rate = 0;

        var source = '../../icons/rate' + rate + '.png';
        return (
            <div>
                <img src={source} alt='rating' className='menu-icon mx-auto w-10 pl-2'></img>
            </div>
        )
    }

    //user info pop-up
    userInfo(data) {
        return (
            <div className='d-flex flex-wrap' id="modal-main">
                <div className="modal fade main-modal" id={"viewUser" + data.UID}>
                    <div className="modal-dialog">
                        <div className="modal-content">
                            <div className="modal-body">
                                <button type="button" className="close text-white" data-dismiss="modal">&times;</button>
                                <div className='px-2'>
                                    <div className='d-flex justify-content-between'>
                                        <UserAvatar UID={data.UID} name={data.FirstName +" "+ data.LastName} size="10mm"/>
                                    </div>
                                    <p className='mr-auto mt-0'>{data.FirstName} {data.LastName}</p>

                                    {this.props.driver ?
                                        <div className='wx-auto'>
                                            <div className='d-flex justify-content-center'>
                                                {this.rating((this.state.avgRatings.AVGBehaviorAsDriver + this.state.avgRatings.AVGConsistentAsDriver + this.state.avgRatings.AVGDrivingSkills) / 3)}
                                            </div>
                                            <hr className="solid"></hr>
                                            <div className='row ml-2'>
                                                <p>behavior:</p>
                                                {this.rating(this.state.avgRatings.AVGBehaviorAsDriver)}
                                            </div>
                                            <div className='row ml-2'>
                                                <p>concistency:</p>
                                                {this.rating(this.state.avgRatings.AVGConsistentAsDriver)}
                                            </div>
                                            <div className='row ml-2'>
                                                <p>driving skills:</p>
                                                {this.rating(this.state.avgRatings.AVGDrivingSkills)}
                                            </div>
                                        </div>
                                        :
                                        <div className='wx-auto'>
                                            <div className='d-flex justify-content-center'>
                                                {this.rating((this.state.avgRatings.AVGBehavior + this.state.avgRatings.AVGConsistent) / 2)}
                                            </div>
                                            <hr className="solid"></hr>
                                            <div className='row ml-2'>
                                                <p>behavior:</p>
                                                {this.rating(this.state.avgRatings.AVGBehavior)}
                                            </div>
                                            <div className='row ml-2'>
                                                <p>concistency:</p>
                                                {this.rating(this.state.avgRatings.AVGConsistent)}
                                            </div>
                                        </div>
                                    }
                                    {/**
                                    <a className="h4">In Detail</a>
                                    <div className="mb-1">
                                        {this.state.detailedRatings.map((rate) =>
                                            <div key={rate.RatingID}>
                                                {this.props.driver ?
                                                    <div>
                                                        {rate.AVGConsistentAsDriver != null && rate.AVGDrivingSkills != null && rate.AVGBehaviorAsDriver != null ?
                                                            <div>
                                                                <label><b>{rate.creatorUsername}</b></label>
                                                                <div className="d-flex d-inline-flex w-auto">
                                                                    <div className='row d-flex flex-column mx-auto px-2'>
                                                                        <label>behavior</label>
                                                                        {this.rating(rate.AVGBehaviorAsDriver)}
                                                                    </div>
                                                                    <div className='row d-flex flex-column mx-auto px-2'>
                                                                        <label>concistency</label>
                                                                        {this.rating(rate.AVGConsistentAsDriver)}
                                                                    </div>
                                                                    <div className='row d-flex flex-column mx-auto px-2'>
                                                                        <label>concistency</label>
                                                                        {this.rating(rate.AVGDrivingSkills)}
                                                                    </div>
                                                                </div>
                                                                <p>{rate.comment ? rate.comment : ''}</p>
                                                            </div>
                                                            :
                                                            null
                                                        }
                                                    </div>
                                                    :
                                                    <div>
                                                        {rate.AVGConsistent != null && rate.AVGBehavior != null ?
                                                            <div>
                                                                <label><b>{rate.creatorUsername}</b></label>
                                                                <div className="d-flex d-inline-flex w-auto">
                                                                    <div className='row d-flex flex-column mx-auto px-2'>
                                                                        <label>behavior</label>
                                                                        {this.rating(rate.AVGBehavior)}
                                                                    </div>
                                                                    <div className='row d-flex flex-column mx-auto px-2'>
                                                                        <label>concistency</label>
                                                                        {this.rating(rate.AVGConsistent)}
                                                                    </div>
                                                                </div>
                                                                <p>{rate.comment ? rate.comment : ''}</p>
                                                            </div>
                                                            :
                                                            null
                                                        }
                                                    </div>
                                                }

                                            </div>
                                        )}
                                    </div>
                                     */}
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        )
    }

    render() {
        return (
            <div>
                {this.userInfo(this.props.data)}
            </div>
        )
    }

}

export default ViewRatings;
