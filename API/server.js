const express = require('express')
const webSocketServer = require('websocket').server;
const http = require('http');
const server = http.createServer();
const app = express();


const bodyParser = require('body-parser');
var cors = require('cors');
var cookieParser = require('cookie-parser');
var port = process.env.PORT || 3305;

server.listen('8000');

var routes = require('./routes/appRoutes.js');
var connectToDB = require('./model/db.js');

//connect to database
connectToDB.connection;

console.log('API server started on: ' + port);
app.use(cors());
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: false }));


const wsServer = new webSocketServer({
  httpServer: server
});

// Generates unique ID for every new connection
const getUniqueID = () => {
  const s4 = () => Math.floor((1 + Math.random()) * 0x10000).toString(16).substring(1);
  return s4() + s4() + '-' + s4();
};

//maintaining all active connections in this object
const clients = [];


const sendMessage = (json, rid) => {
  // sends the current data to all connected clients
  clients.map((client) => {
    if (client.rid === rid)
      client.connection.sendUTF(json);
  });
}

wsServer.on('request', function (request) {
  console.log((new Date()) + ' Recieved a new connection from origin ' + request.origin + '.');
  var userID;

  const connection = request.accept(null, request.origin);
  connection.on('message', function (mssg) {
    if (mssg.type === 'utf8') {
      const dataFromClient = JSON.parse(mssg.utf8Data);

      if (dataFromClient.type == "messagesent") {
        var Message = dataFromClient.Message;
        var Username = dataFromClient.Username;
        var rid = dataFromClient.RID;

        var newMessage = [rid, Username, Message];
        connectToDB.query("INSERT INTO Messages(RID, Username, message) "
          + "VALUE (?, ?, ?)",
          newMessage, function (err, res) {
            if (err) {
              console.log("error: ", err);
            }
          });
        sendMessage(JSON.stringify({ Message, Username }), rid);
      } else if (dataFromClient.type == "userlogin") {
        userID = getUniqueID() + 0;
        var rid = dataFromClient.RID;
        clients.push({ id: [userID], rid, connection });

        connectToDB.query("SELECT * "
          + "FROM messages "
          + "WHERE RID = ? "
          + "ORDER BY timestamp ASC",
          rid, function (err, res) {
            if (err) {
              console.log("error: ", err);
            } else {
              if(res.length)
                res.map((mssg) => connection.sendUTF(JSON.stringify(mssg)));
            }
          });
        console.log('connected: ' + userID);
      }
    }
  });

  // user disconnected
  connection.on('close', function (connection) {
    var userID = 1;
    console.log((new Date()) + " Peer " + userID + " disconnected.");
    delete clients[userID];
  });
});


app.use(cookieParser());

routes(app);
app.listen(port);

