'user strict';
var sql = require('./db.js');
var url = require('url');

//areas object constructor
var Area = function(area){
	this.area = area.area;
}

Area.getAreaByName = function(req, result){
	var name = req.params.name;
	name = name.replace("+", " ");
	sql.query("SELECT AID "
			+"FROM Areas "
			+"WHERE Name = ? ",
			name, function(err, res){
		if(err){
			console.log("error: ", err);
			result(err, null);
		}else{
			if(res.length == 0)
				result(null, "this area doesn't exist");
			else
				result(null, res);
		}
	});
};

Area.getAreaById = function(req, result){
	var id = req.params.aid;
	id = id.replace("+", " ");
	sql.query("SELECT Name "
			+"FROM Areas "
			+"WHERE AID = ? ",
			id, function(err, res){
		if(err){
			console.log("error: ", err);
			result(err, null);
		}else{
			if(res.length == 0)
				result(null, "this area doesn't exist");
			else
				result(null, res);
		}
	});
};

Area.getAllAreas = function(result){
	sql.query("SELECT * "
			+"FROM Areas", function(err, res){
		if(err) {
            console.log("error: ", err);
            result(null, err);
        }else
            result(null, res);
	});
};

Area.postArea = function(areaName, result){
	sql.query("INSERT INTO Areas(Name) "
			+"VALUE (?)",
			areaName, function(err, res){
		if(err){
			console.log("error: ", err);
			result(err, null);
		}else{
			console.log('new area id: ', res);
			result(null, res);
		}
	});
};

Area.deleteArea = function(area, result){
	sql.query("DELETE FROM Areas "
			+"WHERE Name = ?", 
			area, function(err, res){
		if(err){
			console.log("error: ", err);
			result(err, null);
		}else{
			console.log('area deleted: ', res);
			result(null, res);
		}
	});
};

module.exports = Area;