'user strict';
var sql = require('./db.js');
var url = require('url');
var config = require('../config');

var bcrypt = require('bcryptjs');
const jwt = require('jsonwebtoken');

//users object constructor
var User = function (user) {
	this.user = user.user;
}

User.login = function (req, result) {
	//gets given credentials
	var user = new Object();
	if (req.query.login.match(/^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/)) {
		user.login = req.query.login;
		user.type = 'Email';
	} else {
		user.login = req.query.login;
		user.type = 'Username';
	}
	user.password = req.query.password;

	//checks if user exists
	sql.query("SELECT UID, Username, Email, Password "
		+ "FROM Users "
		+ "WHERE (Email = ? OR Username = ?)", [user.login, user.login], function (err, res) {
			if (err)
				result(err, null);
			else if (res.length == 0) {
				result(null, { auth: false, status: 'this user was not found', token: null });
			} else {
				var registeredUser = JSON.parse(JSON.stringify(res[0]));
				var passwordIsValid = bcrypt.compareSync(user.password, registeredUser['Password']);

				if (!passwordIsValid) {
					result(null, { auth: false, status: 'invalid password!', token: null });
					return;
				}

				//creates json web token
				jwt.sign({ result }, config.secret, { expiresIn: '28800s' }, (err, token) => {
					result(null, { auth: true, status: 'user loged in', token, UID: res[0]['UID'] });
				});
			}
		});
};

User.logout = function (req, result) {
	result(null, { auth: false, token: null });
}

User.getUserByUID = function (req, result) {
	var uid = req.params.uid;
	sql.query("SELECT * "
		+ "FROM Users "
		+ "WHERE UID = ? ",
		uid, function (err, res) {
			if (err)
				result(err, null);
			else
				result(null, res);
		});
};

User.getUsernameByUID = function (req, result) {
	var uid = req.params.uid;
	sql.query("SELECT Username "
		+ "FROM Users "
		+ "WHERE UID = ? ",
		uid, function (err, res) {
			if (err)
				result(err, null);
			else
				result(null, res);
		});
};

User.getAvatar = function (req, result) {
	sql.query("SELECT Avatar "
		+ "FROM Users "
		+ "WHERE UID = ?", req,
		function (err, res) {
			if (err) {
				result(err, null);
			} else {
				result(null, res);
			}
		});
};

User.getAllUsers = function (result) {
	sql.query("SELECT * "
		+ "FROM Users",
		function (err, res) {
			if (err) {
				result(err, null);
			} else {
				result(null, res);
			}
		});
};

User.postUser = function (query, result) {
	var hashedPassword = bcrypt.hashSync(query.Password, 8);
	var data = [query.Username, hashedPassword, query.LastName, query.FirstName, query.Email, true];

	sql.query("SELECT COUNT(Username) AS cnt FROM Users WHERE Username = ?",
		query.Username,
		function (err, res) {
			if (err)
				result(err, null);
			else {
				if (res[0].cnt == 0) {
					sql.query("INSERT INTO Users(Username, Password, LastName, FirstName, Email, Active)" +
						"VALUES (?, ?, ?, ?, ?, ?)",
						data,
						function (err, res) {
							if (err)
								result(err, null);
							else
								result(null, { auth: true });
						});
				} else
					result(null, { err_mssg: 'user already exists' });
			}
		});
};

User.putAvatar = function (uid, filename) {
	var data = [filename, uid];

	sql.query("UPDATE Users "
		+ "SET Avatar = ? "
		+ "WHERE UID = ?",
		data,
		function (err, res) {
			if (err)
				console.log(err, null);
		});
};

User.putUser = function (req, result) {
	var user = req[0];
	var path = req[1];
	var sqlQuery = "UPDATE Users SET ";
	var data = [];
	//update password
	if (path.query.Password) {
		sqlQuery = sqlQuery.concat("Password = ? WHERE UID = ?");
		var hashedPassword = bcrypt.hashSync(path.query.Password, 8);
		data.push(hashedPassword);
		data.push(user);

		sql.query(sqlQuery, data, function (err, res) {
			if (err)
				result(err, null);
			else
				result(null, res);
		});
	}
	//update address
	if (path.query.Address) {
		sqlQuery = sqlQuery.concat("Address = ? WHERE UID = ?");
		data.push(path.query.Address);
		data.push(user);

		sql.query(sqlQuery, data, function (err, res) {
			if (err)
				result(err, null);
			else
				result(null, res);
		});
	}
	//update username
	if (path.query.Username) {
		sql.query("SELECT COUNT(Username) AS cnt FROM Users WHERE Username = ?", path.query.Username, function (err, res) {
			if (err)
				result(err, null);
			else {
				if (res[0].cnt == 0) {
					sqlQuery = sqlQuery.concat("Username = ? WHERE UID = ?");
					data.push(path.query.Username);
					data.push(user);
					sql.query(sqlQuery, data, function (err, res) {
						if (err)
							result(err, null);
						else
							result(null, res);
					});
				} else
					result(null, { err_mssg: 'user already exists' });
			}
		});

	}
};

User.deleteUser = function (userId, result) {
	sql.query("DELETE FROM Users"
		+ " WHERE UID = ?",
		userId, function (err, res) {
			if (err)
				result(err, null);
			else
				result(null, res);
		});
};

module.exports = User;