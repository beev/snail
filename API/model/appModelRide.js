'user strict';
var sql = require('./db.js');

//ride's object constructor
var Ride = function (ride) {
	this.ride = ride.ride;
}

Ride.getRide = function (req, result) {
	var rid = req[1].params.rid;
	var pid, participantsTable, postTable;
	var getRideinfo, getParticipants;

	getRideinfo = "SELECT * FROM Rides WHERE RID = ? "
	sql.query(getRideinfo, rid, function (err, res) {
		if (err) {
			console.log("error: ", err);
			result(err, null);
		} else {
			if (res[0].PIDslug !== null) { //if ride was a slug post 
				pid = res[0].PIDslug;
				participantsTable = "slugpostparticipants";
				postTable = 'postslug';
			} else if (res[0].PIDshell !== null) { //if ride was a shell post
				pid = res[0].PIDshell;
				participantsTable = "shellpostparticipants";
				postTable = 'postshell';
			}

			getParticipants = "SELECT part.*, u.Username, u.FirstName, u.LastName, u.Ratings, u.Avatar, post.StartingArea, post.DestinationArea "
				+ "FROM " + participantsTable + " AS part "
				+ "INNER JOIN Users AS u "
				+ "ON part.UID = u.UID "
				+ "INNER JOIN " + postTable + " AS post "
				+ "ON part.PID = post.PID "
				+ "WHERE part.PID = " + pid;

			//get ride's and users' basic info
			sql.query(getParticipants, function (err, res) {
				if (err) {
					console.log("error: ", err);
					result(err, null);
				} else {
					result(null, res);
				}
			})
		}
	});
};

Ride.createRide = function (req, result) {
	var checkQuery, participantsTable, pid, postTable, participants;
	var createRidequery;

	createRidequery = 'INSERT INTO Rides';

	//setting up data, depending on given post type
	if (req.query.type == 'Slug') {
		participantsTable = 'shellpostparticipants';
		pid = 'r.PIDslug';
		postTable = 'PostSlug';
		participants = 'p.NumberOfAvailableSeats BETWEEN 1 AND 4';
		createRidequery = createRidequery + "(PIDslug, PIDshell)";
	} else if (req.query.type == 'Shell') {
		participantsTable = 'slugpostparticipants';
		pid = 'r.PIDshell';
		postTable = 'PostShell';
		participants = 'p.NumberOfParticipants BETWEEN 1 AND 5';
		createRidequery = createRidequery + "(PIDshell, PIDslug)";
	}
	createRidequery = createRidequery + " VALUES(?, Null)"

	//first, check if ride already exists
	checkQuery = "SELECT RID "
		+ "FROM rides AS r "
		+ "INNER JOIN " + postTable + " AS p "
		+ "ON p.PID = " + pid
		+ " WHERE " + participants + " AND " + pid + "=" + req.query.PID;

	sql.query(checkQuery, function (err, res) {
		if (err) {
			console.log("error: ", err);
			result(null, err);
		} else {
			if (res[0]) { //if a ride already exists, update it
				result(null, { rid: res[0].RID }); //return existing rid
			} else { //if there is no ride, create one
				sql.query(createRidequery, req.query.PID, function (err, res) {
					if (err) {
						console.log("error: ", err);
						result(null, err);
					} else {
						result(null, { rid: res.insertId });//return new rid
					}
				});
			}
		}
	});
};

Ride.updateRide = function (req, result) {
	var user = req[0];
	var request = req[1];
	var rid = request.params.rid;
	var sqlQuery; //main query
	var data = []; //for main query
	var query2, query3, query4; //more queries
	var pid, tableParticipants, tablePost; //for secondary query
	var restUsers = []; //users that left in the post (when driver leaves)
	var DestinationArea, StartingArea, postCreator, driverUID, userLeft;
	var tmp;

	//get all ride's participants and info
	sqlQuery = "SELECT PIDslug, PIDshell FROM rides "
		+ "WHERE RID = ?";
	sql.query(sqlQuery, rid, function (err, res) { //get Ride
		if (err) {
			console.log("error: ", err);
			result(err, null);
		} else {
			if (res[0].PIDshell !== null) { //if PIDshell in rides in NOT null
				pid = res[0].PIDshell;
				tableParticipants = 'shellpostparticipants';
				tablePost = 'postshell';
			} else { //if PIDslug in rides in NOT null
				pid = res[0].PIDslug;
				tableParticipants = 'slugpostparticipants';
				tablePost = 'postslug';
			}
		}

		//a user left
		if (request.query.leftUID) {
			query2 = "SELECT * FROM " + tableParticipants
				+ " WHERE PID = ?; SELECT StartingArea, DestinationArea, UID FROM " + tablePost + " WHERE PID = ?";
			sql.query(query2, [pid, pid], function (err, res) {
				if (err) {
					console.log("error: ", err);
					result(err, null);
				} else {
					//get the remaining users' uid
					res[0].map((users) => {
						if (users.UID !== +request.query.leftUID)
							restUsers.push(users.UID);
						else
							userLeft = users.UID
						if (users.driver)
							driverUID = users.UID;
					});
					//get starting and destinaton area
					res[1].map((areas) => { StartingArea = areas.StartingArea; DestinationArea = areas.DestinationArea; postCreator = areas.UID });

					if (userLeft == user && userLeft == request.query.leftUID) { //if user left is userLoggedIn
						if (driverUID === userLeft && restUsers.length >= 1) { //if user left is the driver and there is at least 1 passenger left
							query3 = "";
							//create a new shell post for remaining users
							query3 = query3.concat("INSERT INTO PostShell(UID, StartingArea, DestinationArea, NumberOfParticipants, Active) VALUES(?, ?, ?, ?, ?);");
							data = [restUsers[0]/**give a random user as the one who posted*/, StartingArea, DestinationArea, restUsers.length, true];
							//delete previous existing post
							query3 = query3.concat(" DELETE FROM ");
							query3 = query3.concat(tablePost);
							query3 = query3.concat(" WHERE PID = ?;")
							data.push(pid);
							//delete previous post's records in participants' table
							query3 = query3.concat(" DELETE FROM ");
							query3 = query3.concat(tableParticipants);
							query3 = query3.concat(" WHERE PID = ?")
							data.push(pid);

							sql.query(query3, data, function (err, res) {
								if (err) {
									console.log("error: ", err);
									result(err, null);
								} else {
									//update participants' table
									query4 = "INSERT INTO shellpostparticipants(UID, PID, driver) VALUES";

									restUsers.map((user) => {
										tmp = "(" + user + ", " + res[0].insertId + ", 0), ";
										query4 = query4.concat(tmp);
									});
									query4 = query4.substring(0, query4.length - 2);
									console.log(query4);
									//delete ride
									query4 = query4.concat("; DELETE FROM rides WHERE rid = ?");
									sql.query(query4, rid, function (err, res) {
										if (err) {
											console.log("error: ", err);
											result(err, null);
										} else {
											result(null, res);
										}
									});
								}
							});
						} else if (driverUID !== userLeft && restUsers.length >= 1) { //if user left is a passenger and there is at least 1 person left
							if (postCreator == request.query.leftUID) { //if user left is the one who created prev Shell post 
								query3 = "";
								if (driverUID) {//if the ride had a driver
									//create a new slug post for remaining users
									query3 = query3.concat("INSERT INTO PostSlug(UID, StartingArea, DestinationArea, NumberOfAvailableSeats, Active) VALUES(?, ?, ?, ?, ?);");
									data = [driverUID, StartingArea, DestinationArea, 5 - restUsers.length, true];
								} else {//if the ride had no driver
									//create a new slug post for remaining users
									query3 = query3.concat("INSERT INTO PostShell(UID, StartingArea, DestinationArea, NumberOfParticipants, Active) VALUES(?, ?, ?, ?, ?);");
									data = [restUsers[0], StartingArea, DestinationArea, restUsers.length, true];
								}
								//delete previous existing post
								query3 = query3.concat(" DELETE FROM ");
								query3 = query3.concat(tablePost);
								query3 = query3.concat(" WHERE PID = ?;")
								data.push(pid);
								//delete previous post's records in participants' table
								query3 = query3.concat(" DELETE FROM ");
								query3 = query3.concat(tableParticipants);
								query3 = query3.concat(" WHERE PID = ?")
								data.push(pid);


								sql.query(query3, data, function (err, res) {
									if (err) {
										console.log("error: ", err);
										result(err, null);
									} else {
										//update participants' table
										query4 = "INSERT INTO slugpostparticipants(UID, PID, driver) VALUES";

										restUsers.map((user) => {
											if (user == driverUID)
												tmp = "(" + user + ", " + res[0].insertId + ", 1), ";
											else
												tmp = "(" + user + ", " + res[0].insertId + ", 0), ";
											query4 = query4.concat(tmp);
										});
										query4 = query4.substring(0, query4.length - 2);
										console.log(query4);
										//update ride info
										query4 = query4.concat("; UPDATE rides SET PIDshell = NULL, PIDslug = ? WHERE rid = ?");
										sql.query(query4, [res[0].insertId, rid], function (err, res) {
											if (err) {
												console.log("error: ", err);
												result(err, null);
											} else {
												result(null, res);
											}
										});
									}
								});
							} else { //if user left is any other passenger, just update the tables needed
								//update post's capacity
								console.log(tablePost);
								query3 = "UPDATE ";
								query3 = query3.concat(tablePost);
								if (tablePost == 'postshell') {
									query3 = query3.concat(" SET NumberOfParticipants = ?");
									data = [restUsers.length, pid, restUsers.length];
									query3 = query3.concat(", Active = 1 WHERE PID = ? AND ? BETWEEN 1 AND 5;")
								} else {
									query3 = query3.concat(" SET NumberOfAvailableSeats = ?");
									data = [5 - restUsers.length, pid, 5 - restUsers.length];
									query3 = query3.concat(", Active = 1 WHERE PID = ? AND ? BETWEEN 1 AND 4;")
								}

								//delete previous user's records in participants' table
								query3 = query3.concat(" DELETE FROM ");
								query3 = query3.concat(tableParticipants);
								query3 = query3.concat(" WHERE PID = ? AND UID = ?;")
								data.push(pid);
								data.push(request.query.leftUID);

								//if only one user is left, delete ride
								if (restUsers.length == 1) {
									query3 = query3.concat(" DELETE FROM rides WHERE rid = ?");
									data.push(rid);
								}

								sql.query(query3, data, function (err, res) {
									if (err) {
										console.log("error: ", err);
										result(err, null);
									} else {
										result(null, res);
									}
								});

							}
						} else if (restUsers.length == 0) { //if no more users left 
							//deactivate prev post
							query3 = "UPDATE ";
							query3 = query3.concat(tablePost);
							query3 = query3.concat(" SET Active = 0 WHERE PID = ?;")
							data = [pid];
							//delete previous post's records in participants' table
							query3 = query3.concat(" DELETE FROM ");
							query3 = query3.concat(tableParticipants);
							query3 = query3.concat(" WHERE PID = ?;")
							data.push(pid);
							//delete ride and it's records
							query3 = "DELETE FROM Rides WHERE RID = ?;";
							data.push(rid);

							sql.query(query3, data, function (err, res) {
								if (err) {
									console.log("error: ", err);
									result(err, null);
								} else {
									console.log(res);
									result(null, res);
								}
							});
						}
					}
				}
			});
		} else { //ride completed
			var timestamp = new Date();
			//update ride as copleted
			sqlQuery = "UPDATE Rides SET ";
			if (request.query.completed) {
				sqlQuery = sqlQuery.concat("Completed = ?, TimeFinnished = ? ");
				data.push(request.query.completed);
				data.push(timestamp);
			}

			sqlQuery = sqlQuery.concat("WHERE RID = ?;");
			data.push(rid);

			//clear participants' table
			sqlQuery = sqlQuery.concat(" DELETE FROM ");
			sqlQuery = sqlQuery.concat(tableParticipants);
			sqlQuery = sqlQuery.concat(" WHERE pid = ?;");
			data.push(pid);

			//delete post
			sqlQuery = sqlQuery.concat(" DELETE FROM ");
			sqlQuery = sqlQuery.concat(tablePost);
			sqlQuery = sqlQuery.concat(" WHERE pid = ?");
			data.push(pid);

			sql.query(sqlQuery, data, function (err, res) {
				if (err) {
					console.log("error: ", err);
					result(err, null);
				} else
					result(null, res);
			});

		}
	});
};

Ride.getUsersRide = function (req, result) {
	var uid = req.params.uid;
	var pid, participantsTable;
	var getRideinfo, getParticipationTable;

	getParticipationTable = "SELECT PID FROM slugpostparticipants WHERE UID = ?; SELECT PID FROM shellpostparticipants WHERE UID = ? ;"
	sql.query(getParticipationTable, [uid, uid], function (err, res) {
		if (err) {
			console.log("error: ", err);
			result(err, null);
		} else {
			if (res[0][0]) {//user exists in a slug post
				getRideinfo = "SELECT RID FROM Rides WHERE PIDslug = ?";
				pid = res[0][0].PID;
				sql.query(getRideinfo, pid, function (err, res) {
					if (err) {
						console.log("error: ", err);
						result(err, null);
					} else {
						if (res[0])//if user exists in a ride
							result(null, { rid: res[0].RID, pid: pid, type: 'Slug' });
						else //user doesn't exist in a ride but is in a post
							result(null, { rid: null, pid: pid, type: 'Slug' });
					}
				})
			} else if (res[1][0]) { //user exists in a shell post
				getRideinfo = "SELECT RID FROM Rides WHERE PIDshell = ?";
				pid = res[1][0].PID;
				sql.query(getRideinfo, pid, function (err, res) {
					if (err) {
						console.log("error: ", err);
						result(err, null);
					} else
						if (res[0])//if user exists in a ride
							result(null, { rid: res[0].RID, pid: pid, type: 'Shell' });
						else //user doesn't exist in a ride but is in a post
							result(null, { rid: null, pid: pid, type: 'Shell' });
				})
			} else { //user is neither in a post nor a ride
				result(null, { rid: null, pid: null, type: null })
			}
		}
	});
};

module.exports = Ride;