-- phpMyAdmin SQL Dump
-- version 4.9.1
-- https://www.phpmyadmin.net/
--
-- Φιλοξενητής: 127.0.0.1
-- Χρόνος δημιουργίας: 19 Οκτ 2020 στις 13:33:24
-- Έκδοση διακομιστή: 10.4.8-MariaDB
-- Έκδοση PHP: 7.1.32

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Βάση δεδομένων: `snail`
--

-- --------------------------------------------------------

--
-- Δομή πίνακα για τον πίνακα `areas`
--

CREATE TABLE `areas` (
  `AID` int(11) NOT NULL,
  `Name` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Άδειασμα δεδομένων του πίνακα `areas`
--

INSERT INTO `areas` (`AID`, `Name`) VALUES
(0, 'UOC'),
(1, 'Agia Aikaterini'),
(2, 'Agia Eirini Xrisovalantou'),
(3, 'Agia Marina'),
(4, 'Agia Triada'),
(5, 'Agios Dimitrios'),
(6, 'Agios Ioannis'),
(7, 'Agios Ioannis Xostos'),
(8, 'Agios Minas'),
(9, 'Akadimia'),
(10, 'Analipsi'),
(11, 'Ampelokoipoi'),
(12, 'Atsalenio'),
(13, 'Giofiro'),
(14, 'Deilina'),
(15, 'Dimokratias'),
(16, 'Estavromenos'),
(17, 'Therissos'),
(18, 'Kainouria Porta'),
(19, 'Kamaraki'),
(20, 'Kaminia'),
(21, 'Katsampas'),
(22, 'Kipoupoli'),
(23, 'Kommeno Mpenteni'),
(24, 'Korakovouni'),
(25, 'Knossos'),
(26, 'Linto'),
(27, 'Marathitis'),
(28, 'Mastampas'),
(29, 'Mesampelies'),
(30, 'Mpentevi'),
(31, 'Ksirokampos'),
(32, 'Pateles'),
(33, 'Plateia Eleftherias'),
(34, 'Plateia Kornarou'),
(35, 'Poros'),
(36, 'Treis Vagies'),
(37, 'Tria Pefka'),
(38, 'Fortetsa'),
(39, 'Xanioporta'),
(40, 'Xrusopigi'),
(41, 'Ammoudara'),
(42, 'Amnissos'),
(43, 'Vasileies'),
(44, 'Voutes'),
(45, 'Gazi'),
(46, 'Giofurakia'),
(47, 'Dafnes'),
(48, 'Kavroxori'),
(49, 'Kallithea'),
(50, 'Karteros'),
(51, 'Nea Alikarnassos'),
(52, 'Skalani'),
(53, 'Stavrakia'),
(54, 'Foinikia'),
(55, 'Malades'),
(56, 'Gournes');

-- --------------------------------------------------------

--
-- Δομή πίνακα για τον πίνακα `messages`
--

CREATE TABLE `messages` (
  `RID` int(11) NOT NULL,
  `Username` varchar(255) NOT NULL,
  `Message` text NOT NULL,
  `timestamp` timestamp NOT NULL DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Δομή πίνακα για τον πίνακα `postshell`
--

CREATE TABLE `postshell` (
  `PID` int(11) NOT NULL,
  `DatePosted` timestamp NOT NULL DEFAULT current_timestamp(),
  `UID` int(11) NOT NULL,
  `StartingArea` varchar(255) NOT NULL,
  `DestinationArea` varchar(255) NOT NULL,
  `NumberOfParticipants` int(11) DEFAULT NULL,
  `Active` tinyint(1) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Δομή πίνακα για τον πίνακα `postslug`
--

CREATE TABLE `postslug` (
  `PID` int(11) NOT NULL,
  `DatePosted` timestamp NOT NULL DEFAULT current_timestamp(),
  `UID` int(11) NOT NULL,
  `StartingArea` varchar(255) NOT NULL,
  `DestinationArea` varchar(255) NOT NULL,
  `NumberOfAvailableSeats` int(11) NOT NULL,
  `Active` tinyint(1) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Δομή πίνακα για τον πίνακα `ratings`
--

CREATE TABLE `ratings` (
  `RatingID` int(11) NOT NULL,
  `Behavior` int(11) DEFAULT NULL,
  `Consistent` int(11) DEFAULT NULL,
  `DrivingSkills` int(11) DEFAULT NULL,
  `ConsistentAsDriver` int(11) DEFAULT NULL,
  `BehaviorAsDriver` int(11) DEFAULT NULL,
  `comment` text DEFAULT NULL,
  `UIDrated` int(11) NOT NULL,
  `creatorUID` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Δομή πίνακα για τον πίνακα `rides`
--

CREATE TABLE `rides` (
  `RID` int(11) NOT NULL,
  `PIDslug` int(11) DEFAULT NULL,
  `PIDshell` int(11) DEFAULT NULL,
  `TimeStarted` TIMESTAMP DEFAULT NULL DEFAULT current_timestamp(),
  `TimeFinnished` TIMESTAMP DEFAULT NULL DEFAULT current_timestamp(),
  `Completed` tinyint(1) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Δομή πίνακα για τον πίνακα `shellpostparticipants`
--

CREATE TABLE `shellpostparticipants` (
  `PID` int(11) NOT NULL,
  `UID` int(11) NOT NULL,
  `driver` tinyint(1) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Δομή πίνακα για τον πίνακα `slugpostparticipants`
--

CREATE TABLE `slugpostparticipants` (
  `PID` int(11) NOT NULL,
  `UID` int(11) NOT NULL,
  `driver` tinyint(1) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Δομή πίνακα για τον πίνακα `users`
--

CREATE TABLE `users` (
  `UID` int(11) NOT NULL,
  `Username` varchar(20) NOT NULL,
  `Password` varchar(255) NOT NULL,
  `LastName` varchar(255) NOT NULL,
  `FirstName` varchar(255) NOT NULL,
  `Address` varchar(255) DEFAULT NULL,
  `BirthDate` date DEFAULT NULL,
  `Vehicle` int(11) DEFAULT NULL,
  `Email` varchar(255) NOT NULL,
  `Ratings` int(11) DEFAULT NULL,
  `Avatar` varchar(255) DEFAULT NULL,
  `Active` tinyint(1) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Δομή πίνακα για τον πίνακα `vehicles`
--

CREATE TABLE `vehicles` (
  `VID` int(11) NOT NULL,
  `Type` varchar(255) NOT NULL DEFAULT 'car',
  `Brand` varchar(255) NOT NULL,
  `Model` varchar(255) NOT NULL,
  `Colour` varchar(255) NOT NULL,
  `DefaultAvailableSeats` int(11) DEFAULT NULL,
  `UID` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Ευρετήρια για άχρηστους πίνακες
--

--
-- Ευρετήρια για πίνακα `areas`
--
ALTER TABLE `areas`
  ADD PRIMARY KEY (`AID`);

--
-- Ευρετήρια για πίνακα `postshell`
--
ALTER TABLE `postshell`
  ADD PRIMARY KEY (`PID`),
  ADD KEY `UID` (`UID`);

--
-- Ευρετήρια για πίνακα `postslug`
--
ALTER TABLE `postslug`
  ADD PRIMARY KEY (`PID`),
  ADD KEY `UID` (`UID`);

--
-- Ευρετήρια για πίνακα `ratings`
--
ALTER TABLE `ratings`
  ADD PRIMARY KEY (`RatingID`);

--
-- Ευρετήρια για πίνακα `rides`
--
ALTER TABLE `rides`
  ADD PRIMARY KEY (`RID`);

--
-- Ευρετήρια για πίνακα `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`UID`);

--
-- AUTO_INCREMENT για άχρηστους πίνακες
--

--
-- AUTO_INCREMENT για πίνακα `postshell`
--
ALTER TABLE `postshell`
  MODIFY `PID` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT για πίνακα `postslug`
--
ALTER TABLE `postslug`
  MODIFY `PID` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT για πίνακα `ratings`
--
ALTER TABLE `ratings`
  MODIFY `RatingID` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT για πίνακα `rides`
--
ALTER TABLE `rides`
  MODIFY `RID` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=62;

--
-- AUTO_INCREMENT για πίνακα `users`
--
ALTER TABLE `users`
  MODIFY `UID` int(11) NOT NULL AUTO_INCREMENT;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
