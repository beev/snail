'user strict';
var sql = require('./db.js');
var url = require('url');

//rating object constructor
var Rating = function(rating){
	this.rating = rating.rating;
}

//gets user's ratings
Rating.getAllRatings = function(req, result){
	sql.query("SELECT AVG(Behavior) as AVGBehavior, "+
						"AVG(Consistent) as AVGConsistent, "+
						"AVG(DrivingSkills) as AVGDrivingSkills, "+
						"AVG(BehaviorAsDriver) as AVGBehaviorAsDriver, "+
						"AVG(ConsistentAsDriver) as AVGConsistentAsDriver "
            +"FROM Ratings "
			+"WHERE UIDrated = ?;"
			+" SELECT r.*, u.Username AS creatorUsername FROM Ratings AS r INNER JOIN Users AS u ON u.UID = r.creatorUID WHERE UIDrated = ?", 
            [req.params.uid, req.params.uid], function(err, res){
		if(err) {
            console.log("error: ", err);
            result(err, null);
        }else
            result(null, res);
	});
};

//adds a new rating on a user
Rating.postRating = function(req, result){
	var path = req[1];
	var user = req[0];
	var data;
	var query = "";
	if(path.query.role === "drvr"){
		query = "INSERT INTO Ratings(BehaviorAsDriver, ConsistentAsDriver, DrivingSkills, comment, UIDrated, creatorUID) "+
		"VALUES(?, ?, ?, ?, ?, ?)";
		data = [path.query.BehaviorAsDriver, path.query.ConsistentAsDriver, path.query.DrivingSkills, path.query.text, path.params.uid, user];
	}else{
		query = "INSERT INTO Ratings(Behavior, Consistent, comment, UIDrated, creatorUID) "+
		"VALUES(?, ?, ?, ?, ?)";
		data = [path.query.Behavior, path.query.Consistent, path.query.text, path.params.uid, user];
	}
    sql.query(query, data, function(err, res){
		if(err){
			console.log("error: ", err);
			result(err, null);
		}else
			result(null, "rating submited");
	});
};

//updates a rating
Rating.updateRating = function(req, result){
	var path = req[1];
	var user = req[0];
	var data = [];
	var sqlQuery = "UPDATE Ratings SET ";
	var initQueryLength = sqlQuery.length;

	//check if user has access here
	sql.query("SELECT Creator "
		+"FROM Ratings "
		+"WHERE RID = ?", 
		path.params.rid, function(err, res){
			if(err)
				result(err, null);
			else{
				var Creator = JSON.stringify(res[0]["Creator"]);
				if(Creator != user)
					return result(null, "you don't have access here!");
			}
	});

	if(path.query.behavior <= 0 || path.query.behavior > 5 ||
		path.query.consistent <= 0 || path.query.consistent > 5 ||
		path.query.drivingSkills <= 0 || path.query.drivingSkills > 5)
		return result("invalid rating");

	// update behavior rate
	if(path.query.behavior){
		if(sqlQuery.length !== initQueryLength)
			sqlQuery = sqlQuery.concat(", ");
		sqlQuery = sqlQuery.concat("Behavior = ? ");
		data.push(path.query.behavior);
	}
	//update consistent rate
	if(path.query.consistent){
		if(sqlQuery.length !== initQueryLength)
			sqlQuery = sqlQuery.concat(", ");
		sqlQuery = sqlQuery.concat("Consistent = ? ");
		data.push(path.query.consistent);
	}
	//update drivingSkills rate
	if(path.query.drivingSkills){
		if(sqlQuery.length !== initQueryLength)
			sqlQuery = sqlQuery.concat(", ");
		sqlQuery = sqlQuery.concat("drivingSkills = ? ");
		data.push(path.query.drivingSkills);
	}
	sqlQuery = sqlQuery.concat("WHERE RID = ? AND Creator = ?");
	data.push(path.params.rid);
	data.push(user);

    sql.query(sqlQuery, data, function(err, res){
		if(err){
			console.log("error: ", err);
			result(err, null);
		}else
			result(null, "rating updated");
	});
};

//deletes a rating
Rating.deleteRating = function(req, result){
	var data = [req[1].params.rid, req[0]];
	var message;
	//check if user has access here
	sql.query("SELECT Creator "
		+"FROM Ratings "
		+"WHERE RID = ?", 
		path.params.rid, function(err, res){
			if(err)
				result(err, null);
			else{
				var Creator = JSON.stringify(res[0]["Creator"]);
				if(Creator != user)
					return result(null, "you don't have access here!");
			}
	});
	
    sql.query("DELETE FROM Ratings "
            +"WHERE RID = ? AND Creator = ?",
		    data, function(err, res){
		if(err){
			console.log("error: ", err);
			result(err, null);
		}else
			result(null, "rating deleted");
	});
};

module.exports = Rating;