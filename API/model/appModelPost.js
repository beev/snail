'user strict';
var sql = require('./db.js');
var url = require('url');
const jwt = require('jsonwebtoken');
var User = require('../model/appModelUser.js');

//posts object constructor
var Post = function (post) {
	this.post = post.post;
}

//gets every active post
Post.getAllPosts = function (result) {
	sql.query("SELECT * "
		+ "FROM PostShell "
		+ "WHERE Active = 1 "
		+ "UNION SELECT * "
		+ "FROM PostSlug "
		+ "WHERE Active = 1"
		, function (err, res) {
			if (err) {
				result(null, err);
			} else {
				result(null, res);
			}
		});
};

//gets every active Shell-type post
Post.getShell = function (req, result) {
	var sqlQuery = "SELECT p.*, u.Username, u.FirstName, u.LastName, u.Avatar "
		+ "FROM postshell AS p "
		+ "LEFT JOIN users AS u ON p.UID = u.UID "
		+ "WHERE ";
	if (req.params.pid)
		sqlQuery = sqlQuery.concat("p.PID = ? AND ");
	sqlQuery = sqlQuery.concat("p.Active = 1 ORDER BY p.DatePosted DESC");

	sql.query(sqlQuery, req.params.pid, function (err, res) {
		if (err) {
			console.log("error: ", err);
			result(err, null);
		} else {
			result(null, res);
		}
	});
};

//gets every active Slug-type post
Post.getSlug = function (req, result) {
	var sqlQuery = "SELECT p.*, u.Username, u.FirstName, u.LastName, u.Avatar "
		+ "FROM postslug AS p "
		+ "LEFT JOIN users AS u ON p.UID = u.UID "
		+ "WHERE ";
	if (req.params.pid)
		sqlQuery = sqlQuery.concat("p.PID = ? AND ");
	sqlQuery = sqlQuery.concat("p.Active = 1 ORDER BY p.DatePosted DESC");

	sql.query(sqlQuery, req.params.pid, function (err, res) {
		if (err) {
			console.log("error: ", err);
			result(err, null);
		} else
			result(null, res);
	});
};

//creates a new Shell-type post
Post.postShell = function (req, result) {
	var start = req[1].start;
	var destination = req[1].destination;
	var user = req[0];
	var newPID;
	var data = [user, start, destination, 1, true, user, user, start, destination];
	var query1 = "INSERT INTO PostShell(UID, StartingArea, DestinationArea, NumberOfParticipants, Active) ";
	var query2 = 'INSERT INTO shellpostparticipants(PID, UID, driver) '
		+ 'SELECT ?, ?, ? ';

	//checking input
	query1 = query1 + "SELECT ?, ?, ?, ?, ? WHERE " +
		"0 = (SELECT COUNT(*) FROM shellpostparticipants WHERE UID = ?) AND 0 = (SELECT COUNT(*) FROM slugpostparticipants WHERE UID = ?) " +
		"AND EXISTS (SELECT a.AID, b.AID " +
		"FROM Areas a, Areas b " +
		"WHERE a.Name = ? AND b.Name= ?); ";

	console.log(query1);
	sql.query(query1, data, function (err, res) {
		if (err) {
			console.log("error: ", err);
			result(err, null);
		} else {
			console.log(res);
			if (res.affectedRows == 0) //if user is already in another ride
				result(null, { affectedRows: res.affectedRows });
			else {
				newPID = res.insertId;
				sql.query(query2, [newPID, user, false], function (err, res) {
					if (err) {
						console.log("error: ", err);
						result(err, null);
					} else
						result(null, res);
				});
			}
		}
	});
};

//creates a new Slug-type post
Post.postSlug = function (req, result) {
	var start = req[1].start;
	var destination = req[1].destination;
	var seats = req[1].seats;
	var user = req[0];
	var newPID;
	var data = [req[0], start, destination, seats, true, seats, user, user, start, destination];
	var query1 = "INSERT INTO PostSlug(UID, StartingArea, DestinationArea, NumberOfAvailableSeats, Active) ";
	var query2 = 'INSERT INTO slugpostparticipants(PID, UID, driver) '
		+ 'SELECT ?, ?, ? ';

	//checking input
	query1 = query1 + "SELECT ?, ?, ?, ?, ? " +
		"WHERE ? BETWEEN 1 AND 4 " +
		"AND 0 = (SELECT COUNT(*) FROM shellpostparticipants WHERE UID = ?) AND 0 = (SELECT COUNT(*) FROM slugpostparticipants WHERE UID = ?) " +
		"AND EXISTS (SELECT a.AID, b.AID " +
		"FROM Areas a, Areas b " +
		"WHERE a.Name = ? AND b.Name= ?)";

	sql.query(query1, data, function (err, res) {
		if (err) {
			console.log("error: ", err);
			result(err, null);
		} else {
			if (res.affectedRows == 0) //if user is already in another ride
				result(null, { affectedRows: res.affectedRows });
			else {
				newPID = res.insertId;
				sql.query(query2, [newPID, user, true, user, user], function (err, res) {
					if (err) {
						console.log("error: ", err);
						result(err, null);
					} else
						result(null, res);
				});
			}
		}
	});
};

//updates Shell-type post
Post.putShell = function (req, result) {
	var user = req[0];
	var request = req[1];
	var activity = request.query.passengers <= 5 && request.query.passengers > 0 ? 1 : 0;
	var sqlQuery = "UPDATE postshell SET NumberOfParticipants = ?, Active = ? WHERE PID = ? AND ? BETWEEN 1 AND 5";
	var data = [request.query.passengers, activity, request.params.pid, request.query.passengers];
	var data2 = [request.params.pid, user, false, user, user]

	var query2 = 'INSERT INTO shellpostparticipants(PID, UID, driver) '
		+ 'SELECT ?, ?, ? ' //check user is currently not in another ride
		+ 'WHERE 0 = (SELECT COUNT(*) FROM shellpostparticipants WHERE UID = ?) AND 0 = (SELECT COUNT(*) FROM slugpostparticipants WHERE UID = ?)'

	if (request.query.driver === '1') {/**check if unique driver */
		query2 = query2.concat(" AND 0 = (SELECT COUNT(*) FROM shellpostparticipants WHERE driver = 1 AND PID = ?)");
		data2.push(request.params.pid);
		data2[2]= true;
	} else {//check if there is enough space for another passenger
		query2 = query2.concat(" AND (SELECT COUNT(*) FROM shellpostparticipants WHERE driver = 0 AND PID = ?) BETWEEN 1 AND 3");
		data2.push(request.params.pid);
	}

	sql.query(query2, data2, function (err, res) {
		if (err) {
			console.log("error: ", err);
			result(err, null);
		} else {
			if (res.affectedRows == 0){
				console.log(res);
				result(null, { affectedRows: res.affectedRows });
			}else {
				sql.query(sqlQuery, data, function (err, res) {
					if (err) {
						console.log("error: ", err);
						result(err, null);
					} else {
						result(null, res);
					}
				});
			}
		}
	});
};

//updates Slug-type post 
Post.putSlug = function (req, result) {
	var user = req[0];
	var request = req[1];
	var activity = request.query.seats < 5 && request.query.seats > 0 ? 1 : 0;
	
	var sqlQuery = "UPDATE postslug SET NumberOfAvailableSeats = ?, Active = ? WHERE PID = ? AND ? BETWEEN 0 AND 4";
	var data = [request.query.seats, activity, request.params.pid, request.query.seats];
	var data2 = [request.params.pid, user, false, user, user];

	var query2 = 'INSERT INTO slugpostparticipants(PID, UID, driver) '
		+ 'SELECT ?, ?, ? ' //check user is currently not in another ride
		+ 'WHERE 0 = (SELECT COUNT(*) FROM shellpostparticipants WHERE UID = ?) AND 0 = (SELECT COUNT(*) FROM slugpostparticipants WHERE UID = ?)'

	//check if there is enough space for another passenger
	query2 = query2.concat(" AND (SELECT COUNT(*) FROM slugpostparticipants WHERE driver = 0 AND PID = ?) BETWEEN 0 AND 3");
	data2.push(request.params.pid);

	sql.query(query2, data2, function (err, res) {
		if (err) {
			console.log("error: ", err);
			result(err, null);
		} else {
			if (res.affectedRows == 0)
				result(null, { affectedRows: res.affectedRows });
			else {
				sql.query(sqlQuery, data, function (err, res) {
					if (err) {
						console.log("error: ", err);
						result(err, null);
					} else {
						result(null, res);
					}
				});
			}
		}
	});
};

//deletes a Shell-type post
Post.deleteShell = function (req, result) {
	var user = req[0];
	var pid = req[1].params.pid
	sql.query("UPDATE postshell "
		+ "SET Active = 0 "
		+ "WHERE UID = ? AND PID = ?; "
		+ "DELETE FROM shellpostparticipants WHERE PID = ?;",
		[user, pid, pid], function (err, res) {
			if (err) {
				console.log("error: ", err);
				result(err, null);
			} else
				result(null, res);
		});
};

//deletes a Slug-type post
Post.deleteSlug = function (req, result) {
	var user = req[0];
	var pid = req[1].params.pid
	sql.query("UPDATE postslug "
		+ "SET Active = 0 "
		+ "WHERE UID = ? AND PID = ?; "
		+ "DELETE FROM slugpostparticipants WHERE PID = ?;",
		[user, pid, pid], function (err, res) {
			if (err) {
				console.log("error: ", err);
				result(err, null);
			} else
				result(null, res);
		});
};

module.exports = Post;