'use strict';
const express = require('express');
const router = express.Router();
var verifyToken = require('./verifyToken');

var mapAreas = require('../controller/appControllerArea');
var mapUsers = require('../controller/appControllerUser');
var mapPosts = require('../controller/appControllerPost');
var mapRatings = require('../controller/appControllerRating');
var mapRides = require('../controller/appControllerRide');


module.exports = function(app){
	//homepage
	app.get('/', (req, res) => {
		res.send('PORT 5000');
	})
	app.post('/login', mapUsers.login);
	app.post('/logout', mapUsers.logout);
	
	//user Routes
	app.post('/users', mapUsers.addUser); //create a new user
	app.put('/users', verifyToken, mapUsers.updateUser); //update user's info
	app.put('/users/avatar/:uid', verifyToken, mapUsers.updateAvatar); //get user's avatar
	app.get('/users/avatar/:uid', verifyToken, mapUsers.getAvatar); //get user's avatar
	app.get('/users/:uid', verifyToken, mapUsers.viewAUser); //get a user by uid
	app.get('/users', verifyToken, mapUsers.listAllUsers); //get all users
	app.get('/users/user/:uid', verifyToken, mapUsers.getUsername); //get username
	app.delete('/users/:uid', verifyToken, mapUsers.deleteUser); //delete a user
	app.delete('/users/avatar/:uid', verifyToken, mapUsers.deleteAvatar); //delete user's avatar

	//post Routes
	app.get('/posts', verifyToken, mapPosts.getAllPosts);
	app.get('/postsSlug', verifyToken, mapPosts.getSlug); //get all slug posts
	app.get('/postsSlug/:pid', verifyToken, mapPosts.getSlug); //get specific slug post by ID
	app.post('/postsSlug', verifyToken, mapPosts.createSlug); //create a new Slug post
	app.put('/postsSlug/:pid', verifyToken, mapPosts.updateSlug); //update slug post
	app.delete('/postsSlug/:pid', verifyToken, mapPosts.deleteSlug); //delete slug post

	app.get('/postsShell', verifyToken, mapPosts.getShell); //get all shell posts
	app.get('/postsShell/:pid', verifyToken, mapPosts.getShell); //get specific shell post by ID
	app.post('/postsShell', verifyToken, mapPosts.createShell); //create a new Shell post
	app.put('/postsShell/:pid', verifyToken, mapPosts.updateShell); //update shell post
	app.delete('/postsShell/:pid', verifyToken, mapPosts.deleteShell); //delete shell post

	//rating Routes
	app.get('/ratings/:uid', verifyToken, mapRatings.getAllRatings); // get avverage rating of a user
	app.post('/ratings/:uid', verifyToken, mapRatings.addRating); // rate a user
	app.put('/ratings/:rid', verifyToken, mapRatings.updateRating); // update a user's rating
	app.delete('/ratings/:rid', verifyToken, mapRatings.deleteRating); // delete a user's rating

	//ride Routes
	app.get('/rides/user/:uid', verifyToken, mapRides.getUsersRide)
	app.get('/rides/:rid', verifyToken, mapRides.getRide);
	app.post('/rides', verifyToken, mapRides.createRide);
	app.put('/rides/:rid', verifyToken, mapRides.updateRide);

	//areas Routes
	app.get('/areas', mapAreas.listAllAreas); // get all areas
	app.get('/areas/:name', mapAreas.viewAnArea); // get an area by name
	app.get('/areas/:AID', mapAreas.getArea); // get an area by id
	//app.put('/areas/', verifyToken, mapAreas.addArea);
	//app.delete('/areas/:name', verifyToken, mapAreas.deleteArea);

	
};