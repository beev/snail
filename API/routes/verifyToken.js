var jwt = require('jsonwebtoken');
var config = require('../config');
const bodyParser = require('body-parser');

function verifyToken(req, res, next) {
  var token = req.cookies['access-token'];
  if (!token || token === null)
    return res.json({auth: false, message: 'No token provided.' });
    
  jwt.verify(token, config.secret, function(err, decoded) {
    if(err)
      return res.json({auth: false, message: 'Failed to authenticate token.' });
    else
      next();
  });
}


module.exports = verifyToken;