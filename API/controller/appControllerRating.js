'use strict';

var Rating = require('../model/appModelRating.js');

//gets user's ratings
exports.getAllRatings = function(req, res){
    Rating.getAllRatings(req, function(err, result){
        if(err)
            res.send(err);
        res.send(result);
    });
};


//creates a rating
exports.addRating = function(req, res){
    var user = req.cookies['loggedInUser']; //the id of the logged in user
    if(user == req.params.uid){
        res.send("you cannot rate yourself, dummy");
        return;
    }
    Rating.postRating([user, req], function(err, result){
        if(err)
            res.send(err);
        res.send(result);
    });
};

//updates a rating
exports.updateRating = function(req, res){
    var user = req.cookies['loggedInUser']; //the id of the logged in user

    Rating.updateRating([user, req], function(err, result){
        if(err)
            res.send(err);
        res.send(result);
    });
};

//deletes a rating
exports.deleteRating = function(req, res){
    var user = req.cookies['loggedInUser']; //the id of the logged in user
    
    Rating.deleteRating([user, req], function(err, result){
        if(err)
            res.send(err);
        res.send(result);
    });
};