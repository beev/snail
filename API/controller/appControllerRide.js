'use strict';
var Ride = require('../model/appModelRide.js');

exports.getRide = function(req, res){
	var user = req.cookies['loggedInUser']; //the id of the logged in user
	Ride.getRide([user, req], function(err, result){
		if(err)
			res.send(err);
		res.json(result);
	});
};

exports.createRide = function(req, res){
	Ride.createRide(req, function(err, result){
		if(err)
			res.send(err);
		res.json(result);
	});
};

exports.updateRide = function(req, res){	
	var user = req.cookies['loggedInUser']; //the id of the logged in user
	Ride.updateRide([user, req], function(err, result){
		if(err)
			res.send(err);
		res.json(result);
	});
};

exports.getUsersRide = function(req, res){
	var user = req.cookies['loggedInUser']; //the id of the logged in user
	if(req.params.uid !== user) //if logged in user has no access in such information
		return;
	Ride.getUsersRide(req, function(err, result){
		if(err)
			res.send(err);
		res.json(result);
	});
};
