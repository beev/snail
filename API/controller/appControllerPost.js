'use strict';
var Post = require('../model/appModelPost.js');

//gets every active post
exports.getAllPosts = function (req, res) {
	Post.getAllPosts(function (err, result) {
		if (err)
			res.send(err);
		res.json(result);
	});
};

//gets every active Shell-type post
exports.getShell = function (req, res) {
	Post.getShell(req, function (err, result) {
		if (err)
			res.send(err);
		res.json(result);
	});
};

//gets every active Slug-type post
exports.getSlug = function (req, res) {
	Post.getSlug(req, function (err, result) {
		if (err)
			res.send(err);
		res.json(result);
	});
};

//creates a Shell-type post
exports.createShell = function (req, res) {
	var user = req.cookies['loggedInUser']; //the id of the logged in user
	Post.postShell([user, req.query], function (err, result) {
		if (err)
			res.send(err);
		res.json(result);
	});
};

//creates a Slug-type post
exports.createSlug = function (req, res) {
	var user = req.cookies['loggedInUser']; //the id of the logged in user
	Post.postSlug([user, req.query], function (err, result) {
		if (err)
			res.send(err);
		res.json(result);
	});
};

//updates Shell-type post
exports.updateShell = function (req, res) {
	var user = req.cookies['loggedInUser']; //the id of the logged in user
	Post.putShell([user, req], function (err, result) {
		if (err)
			res.send(err);
		res.json(result);
	});
};

//updates Slug-type post
exports.updateSlug = function (req, res) {
	var user = req.cookies['loggedInUser']; //the id of the logged in user
	Post.putSlug([user, req], function (err, result) {
		if (err)
			res.send(err);
		res.json(result);
	});
};

//deletes a post
exports.deleteShell = function (req, res) {
	var user = req.cookies['loggedInUser']; //the id of the logged in user
	Post.deleteShell([user, req], function (err, result) {
		if (err)
			res.send(err);
		res.send("shell post deleted");
	});
};

exports.deleteSlug = function (req, res) {
	var user = req.cookies['loggedInUser']; //the id of the logged in user	
	Post.deleteSlug([user, req], function (err, result) {
		if (err)
			res.send(err);
		res.send("slug post deleted");
	});
};