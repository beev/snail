'use strict';
var User = require('../model/appModelUser.js');
const jwt = require('jsonwebtoken');
const bodyParser = require('body-parser');
var multer = require('multer');
const fs = require('fs');


exports.login = function (req, res) {
	User.login(req, function (err, result) {
		if (err)
			res.send(err);
		else {
			res.cookie('access-token', result['token'], { httpOnly: false });
			res.cookie('loggedInUser', result['UID'], { httpOnly: false });
			res.send(result);
		}
	});
	return;
};

exports.logout = function (req, res) {
	User.logout(req, function (err, result) {
		if (err)
			res.send(err);
		else {
			res.cookie('access-token', 0, { httpOnly: false });
			res.cookie('loggedInUser', -1, { httpOnly: false });
			res.send(result);
		}
	})
};

exports.listAllUsers = function (req, res) {
	User.getAllUsers(function (err, result) {
		if (err)
			res.send(err);
		if (result.length == 0)
			res.send('no registered users');
		else {
			console.log(result);
			res.json(result);
		}
	});
	return;
};

exports.viewAUser = function (req, res) {
	User.getUserByUID(req, function (err, result) {
		if (err)
			res.send(err);
		if (result.length == 0)
			res.send('no such user registered');
		else
			res.json(result);
	});
};

exports.getUsername = function (req, res) {
	User.getUsernameByUID(req, function (err, result) {
		if (err)
			res.send(err);
		if (result.length == 0)
			res.send('no such user registered');
		else
			res.json(result);
	});
};

exports.getAvatar = function (req, res) {
	var user = req.params.uid;
	var path = "./public/avatars";
	User.getAvatar(user, function (err, result) {
		if (err)
			res.send(err);
		else {
			if (result[0] && result[0].Avatar !== "NULL" && result[0].Avatar !== null)
				res.sendFile("/" + result[0].Avatar, { root: path });
			else
				res.json({avatar:"NULL"});
		}
	});
};

exports.addUser = function (req, res) {
	User.postUser(req.query, function (err, result) {
		if (err) {
			res.send(err);
		} else {
			res.json(result);
		}
	});
};

exports.updateUser = function (req, res) {
	var user = req.cookies['loggedInUser']; //the id of the loged in user
	User.putUser([user, req], function (err, result) {
		if (err)
			res.send(err);
		else
			res.json(result);
	});
};

exports.updateAvatar = function (req, res) {
	var user = req.cookies['loggedInUser']; //the id of the loged in user
	//instances for saving files to file system
	var storage, upload;
	var filename = 'user' + user + ".";

	storage = multer.diskStorage({
		destination: function (req, file, cb) {
			cb(null, './public/avatars')
		},
		filename: function (req, file, cb) {
			cb(null, filename + file.originalname.split('.').pop()) //save file with modified name but same extension
		}
	});

	upload = multer({ storage: storage }).single('file');

	upload(req, res, function (err) {
		if (err instanceof multer.MulterError) {
			console.log(err)
		} else if (err) {
			console.log(err)
		}
		User.putAvatar(user, filename + req.file.originalname.split('.').pop());
		return res.status(200).send(req.file)
	});


};

exports.deleteAvatar = function (req, res) {
	var user = req.cookies['loggedInUser']; //the id of the loged in user
	var path = './public/avatars/' + req.query.avatarName;

	console.log('req :' + req.query.avatarName);
	try {
		fs.unlinkSync(path);
		User.putAvatar(user, 'NULL');
		return res.status(200);
	} catch (err) {
		console.error(err)
	}

};

exports.deleteUser = function (req, res) {
	var user = req.cookies['loggedInUser']; //the id of the loged in user
	var userId = req.params.uid; //the id requested to be deleted
	if (userId !== user)
		return res.send('You don\'t have access here!');
	User.deleteUser(userId, function (err, result) {
		if (err)
			res.send(err);
		res.send('ok');
	});
	res.cookie('access-token', null, { httpOnly: false });
};
