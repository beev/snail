'use strict';
var Area = require('../model/appModelArea.js');

exports.listAllAreas = function(req, res){
	Area.getAllAreas(function(err, areas){
		if(err)
			res.send(err);
		res.send(areas);
	});
};

exports.viewAnArea = function(req, res){
	Area.getAreaByName(req, function(err, area){
		if(err)
			res.send(err);
		res.send(area);
	});
};

exports.getArea = function(req, res){
	Area.getAreaById(req, function(err, area){
		if(err)
			res.send(err);
		res.send(area);
	});
};

exports.addArea = function(req, res){
	Area.postArea(req.query.name, function(err, area){
		if(err)
			res.send(err);
		res.json(area);
	});
};

exports.deleteArea = function(req, res){	
	Area.deleteArea(req.params.aid, function(err, area){
		if(err)
			res.send(err);
		res.json(area);
	});
};